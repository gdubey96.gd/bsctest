import Header from "./Header";
import React, { Component } from "react";
import "../Styles/ecolizer.css";
import Footer from "./Footer";

class Ecolizer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      web3: null,
      radioToggle: {
        data: null,
        toggle: false
      }
    };
  }

  componentDidMount() {
    this.setState({
      web3: this.props.state,
    });
  }

  onChangeWeb3 = (web3) => {
    this.setState(
      {
        web3: web3,
      },
      () => {
        // this.startingFunctions();
      }
    );
  };

  handleRadioClick = async(name) => {
    this.setState(prevState => {
      let radioToggle = Object.assign({}, prevState.radioToggle);
      radioToggle.data = name;
      return { radioToggle };
    })
  }

  popup_energy = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div")
        .classList.remove("overlay_flight_traveldil_show");
  };

  popup_energy2 = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div2")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div2")
        .classList.remove("overlay_flight_traveldil_show");
  };

  popup_energy3 = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div3")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div3")
        .classList.remove("overlay_flight_traveldil_show");
  };

  popup_energy4 = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div4")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div4")
        .classList.remove("overlay_flight_traveldil_show");
  };

  popup_energy5 = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div5")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div5")
        .classList.remove("overlay_flight_traveldil_show");
  };

  render() {
    const { web3, radioToggle } = this.state;
    return (
      <>
        {<Header
            propData={null}
            onChangeWeb3={(web3) => this.onChangeWeb3(web3)}
          />}
          <div className="container">
              <div className="row">
                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <p className="coming-soon">Comming Soon...</p>
                </div>
              </div>

          </div>
          <Footer />
      </>
    );
  }
}

export default Ecolizer;
