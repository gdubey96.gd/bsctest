import React, { Component } from "react";
import "../Styles/orders.css";
import Footer from "./Footer";
import Header2 from "./Header2";
import ERC20Token from "../ABI/ERC20.json";
import MasterOppurtunity from "../ABI/masterOppurtunity.json";
import singleOppurtunity from "../ABI/singleOppurtunity.json";
import WalletConnectProvider from "@walletconnect/web3-provider";
import oppurtunityNFTContract from "../ABI/oppurtunityNFTContract.json";
import Data from "../Constants/contract";
import tokenData from "../Constants/token";
import { TokensEarn } from "../Constants/contract";
import Web3 from "web3";
import Header from "./Header";
import Popup from "./Popup";
import WBTC from "../images/B.png";
import ECO from "../images/eco.png";
import ETH from "../images/ether.png";
import OMC from "../images/omc.png";
import ORME from "../images/ormeus.png";
import USDT from "../images/busd.svg";
import WBNB from "../images/binance.png";
import Loader from "./Loader";
import { withRouter } from "react-router";
import Banner2 from "../images/EcoCelium_promobanner-01.png";
import Boxes from "./Boxes";
import OrdersAllCoins from "./OrdersAllCoins";
import OrdersTable from "./OrdersTable";
import OrderCards from "./OrderCards";
import Completed from "./Completed";
import Failed from "./Failed";
import Loading from "./Loading";

import * as ethers from "ethers";

class Orders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Images: [ECO, ORME, USDT, WBTC, ETH, WBNB, OMC],
      ImagesPrice: [ECO, OMC, ORME, USDT, WBTC, ETH, WBNB],
      usdValueTokenList: [
        "ormeus-ecosystem",
        "ormeus-cash",
        "ormeuscoin",
        "binance-usd",
        "wrapped-bitcoin",
        "ethereum",
        "binancecoin",
      ],
      web3: null,
      account: null,
      contractAddress: "0x7E1b859F6C792cABBc2d5581703a3ee1d09674Fd",
      isLoading: false,
      contractInit: null,
      oppurtunities: [],
      oppurtunitiesTokens: [],
      showMonths: false,
      monthToken: null,
      selectedMonthAddress: null,
      index: null,
      amount: 0,
      nftBalance: null,
      showAllowance: false,
      allowanceAmount: 0,
      tokenIdDetails: [],
      tokenHistory: [],
      formTokenId: null,
      tokenRadioTokenIndex: null,
      monthRadioIndex: null,
      pops: false,
      message: null,
      usdValueList: [],
      selectedMonthValue: null,
      tokenBalance: 0,
      apyData: 0,
      apyArray: [],
      apyText: true,
      apyPercentage: true,
      orderCardsArray: [],
      priceDetailsArray: [],
      pops1: false,
      pops2: false,
      pops4: false,
      popupToken: null,
      transactionDetails: {
        hash: null,
        ETA: null,
        gasLimit: null,
        gasUsed: null,
        gasPrice: null,
      },
      transactionErrors: {
        message: "Transaction Failed!",
        messageError: "Something went wrong! Please try again!",
      },
    };
  }

  componentDidMount() {
    this.fetchUsdValue();
  }

  startingFunctions = async () => {
    this.setState({
      isLoading: true,
    });
    await this.getAccounts();
    await this.initializeContract();
    await this.getAllOppurtunities();
    await this.getAllTokenName();
    await this.getNFTBalance();
    await this.tokenIdData();
    this.setState({
      isLoading: false,
    });
  };

  onChangeWeb3 = (web3, provider) => {
    this.setState(
      {
        web3: web3,
        provider,
      },
      () => {
        this.startingFunctions();
      }
    );
  };

  fetchUsdValue = async () => {
    if (this.state.priceDetailsArray.length == 0) {
      for (let i = 0; i < this.state.usdValueTokenList.length; i++) {
        const element = this.state.usdValueTokenList[i];

        var data = await this.getPrice(element);

        this.setState((prevState) => ({
          priceDetailsArray: [
            ...prevState.priceDetailsArray,
            <OrdersAllCoins data={parseFloat(data).toFixed(4)} i={i} />,
          ],
        }));
      }
    }
  };

  getPrice = async (element) => {
    try {
      var priceData = await fetch(
        `https://api.coingecko.com/api/v3/simple/price?ids=${element}&vs_currencies=USD`
      );

      var jsonPrice = await priceData.json();

      var jsonData = jsonPrice[element] ? jsonPrice[element].usd : 0;

      return jsonData;
    } catch (error) {}
  };

  getAccounts = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var network = await this.state.web3.eth.net.getNetworkType();
      var accounts = await this.state.web3.eth.getAccounts();
      // this.setState({ account: "0x21a29a50F66d5E8DB6E1F700d3987e81C21f259c" });
      this.setState({ account: accounts[0] });
      // this.setState({
      //   account: "0x0ee5f030090a6388db1d28f812690c6cc0484359",
      // });
    }
  };

  initializeContract = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        MasterOppurtunity,
        Data.EarnMasterOpportunity
      );
      this.setState({ contractInit });
    }
  };

  getAllOppurtunities = async () => {
    if (this.state.contractInit != null) {
      var router = await this.state.contractInit.methods
        .getActiveOpportunities()
        .call();
      this.setState({ oppurtunities: router }, () => {
        console.log(
          "this.state.oppurtunities.length :::::::::::::",
          this.state.oppurtunities
        );
      });
    }
  };

  getAllTokenName = async () => {
    this.setState({
      oppurtunitiesTokens: [],
      orderCardsArray: [],
    });
    if (this.state.oppurtunities.length > 0) {
      var obj = {
        name,
        originalName: [],
        address: [],
        singleOppurtunityAddress: [],
        nftAddress: [],
        apy: [],
        month: [],
      };

      for (let i = 0; i < this.state.oppurtunities.length; i++) {
        const element = this.state.oppurtunities[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element
        );

        var name = await contractInit.methods.getName().call();
        var tokenName = await this.checkTokenName(name);
        var tokenMonth = await this.checkTokenMonth(name);
        var address = await contractInit.methods.getTokenAddress().call();
        var nftAddress = await contractInit.methods.getNFTAddress().call();
        var apy = await contractInit.methods.getApy().call();

        var obj = {
          name,
          originalName: [],
          address: [],
          singleOppurtunityAddress: [],
          nftAddress: [],
          apy: [],
          month: [],
        };

        var stateOpport = this.state.oppurtunitiesTokens;

        var tog = await this.tokenExists(
          this.state.oppurtunitiesTokens,
          tokenName
        );

        var ind = this.state.oppurtunitiesTokens.findIndex(
          (d) => d.name === tokenName
        );

        if (tog && ind > -1) {
          stateOpport[ind].originalName.push(name);
          stateOpport[ind].address.push(address);
          stateOpport[ind].singleOppurtunityAddress.push(element);
          stateOpport[ind].nftAddress.push(nftAddress);
          stateOpport[ind].apy.push(apy);
          stateOpport[ind].month.push(tokenMonth);
        } else {
          if (tog != true) {
            obj.name = tokenName;
            obj.originalName.push(name);
            obj.address.push(address);
            obj.singleOppurtunityAddress.push(element);
            obj.nftAddress.push(nftAddress);
            obj.apy.push(apy);
            obj.month.push(tokenMonth);

            this.setState((prevState) => ({
              orderCardsArray: [
                ...prevState.orderCardsArray,
                <OrderCards
                  data={obj}
                  i={stateOpport.length}
                  tokenClick={(e, name, i) => this.tokenClick(e, name, i)}
                  tokenRadioTokenIndex={this.state.tokenRadioTokenIndex}
                  apyText={this.state.apyText}
                  apyPercentage={this.state.apyPercentage}
                  apyData={this.state.apyData}
                />,
              ],
            }));

            this.setState((prevState) => ({
              oppurtunitiesTokens: [...prevState.oppurtunitiesTokens, obj],
            }));
          } else {
            break;
          }
        }
      }
    }
  };

  tokenExists = (arr, name) => {
    return arr.some(function (el) {
      return el.name === name;
    });
  };

  checkTokenName = async (data) => {
    var tokenName;
    for (let i = 0; i < tokenData.tokenNames.length; i++) {
      const element = tokenData.tokenNames[i];
      var value = await data
        .toString()
        .toUpperCase()
        .includes(element.toUpperCase());
      if (value) {
        tokenName = element;
      }
    }
    return tokenName;
  };

  checkTokenMonth = async (data) => {
    var tokenMonth;
    for (let i = 0; i < tokenData.tokenMonths.length; i++) {
      const element = tokenData.tokenMonths[i];
      var value = await data
        .toString()
        .toUpperCase()
        .includes(element.toString().toUpperCase());
      if (value) {
        tokenMonth = element;
      }
    }
    return tokenMonth;
  };

  tokenClick = async (event, token, index) => {
    event.preventDefault();

    this.setState(
      {
        showMonths: true,
        monthToken: token,
        index,
        tokenRadioTokenIndex: index,
        popupToken: token,
      },
      async () => {
        var contractInit = new this.state.web3.eth.Contract(
          ERC20Token,
          TokensEarn[this.state.tokenRadioTokenIndex]
        );

        // var decimal = await contractInit.methods.decimals().call();

        var bal = await contractInit.methods
          .balanceOf(this.state.account)
          .call();

        this.setState({
          tokenBalance: parseInt(bal) / 10 ** 18,
          selectedMonthValue: null,
          monthRadioIndex: null,
          apyData: 0,
        });
      }
    );

    await this.tokenIdData();

    await this.getNFTBalance();
  };

  amountChange = async (event) => {
    event.preventDefault();

    if (this.state.selectedMonthAddress != null) {
      await this.checkAllowance();

      if (this.state.web3 != null) {
        this.setState({
          amount: event.target.value,
        });

        if (
          this.state.allowanceAmount < this.state.amount ||
          this.state.amount == 0
        ) {
          this.setState({
            showAllowance: true,
          });
        } else {
          this.setState({
            showAllowance: false,
          });
        }

        await this.checkAllowance();
      }
    }
  };

  selectedMonth = async (event, address, i, month, index) => {
    event.preventDefault();

    this.setState({
      apyText: month == 6 ? false : true,
    });

    this.setState({
      selectedMonthAddress: address,
      monthRadioIndex: i,
      selectedMonthValue: month,
      apyData: this.state.oppurtunitiesTokens[index].apy[i],
    });

    await this.checkAllowance();
  };

  checkAllowance = async () => {
    var contractInit = new this.state.web3.eth.Contract(
      ERC20Token,
      TokensEarn[this.state.tokenRadioTokenIndex]
    );

    var decimal = await contractInit.methods.decimals().call();
    var amount = await contractInit.methods
      .allowance(this.state.account, this.state.selectedMonthAddress)
      .call();

    this.setState({
      allowanceAmount: amount / 10 ** decimal,
    });
  };

  toFixed = (x) => {
    if (Math.abs(x) < 1.0) {
      var e = parseInt(x.toString().split("e-")[1]);
      if (e) {
        x *= Math.pow(10, e - 1);
        x = "0." + new Array(e).join("0") + x.toString().substring(2);
      }
    } else {
      var e = parseInt(x.toString().split("+")[1]);
      if (e > 20) {
        e -= 20;
        x /= Math.pow(10, e);
        x += new Array(e + 1).join("0");
      }
    }
    return x;
  };

  allowance = async (event) => {
    event.preventDefault();

    if (this.state.selectedMonthAddress != null && this.state.amount != null) {
      var prov = new ethers.providers.Web3Provider(this.state.provider);

      var signer = await prov.getSigner(0);

      var contractInit = new ethers.Contract(
        TokensEarn[this.state.tokenRadioTokenIndex],
        ERC20Token,
        signer
      );

      try {
        var amo = await this.toFixed(10 ** 71);

        var con = contractInit.approve(
          this.state.selectedMonthAddress,
          amo.toString()
        );

        con
          .then(async (data) => {
            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: data.hash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                // gasUsed: data.gasUsed,
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops4: true,
            });

            var set = await prov.waitForTransaction(data.hash);

            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: set.transactionHash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                gasUsed: set.gasUsed.toString(),
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops1: true,
              pops4: false,
              showAllowance: false
            });

            setTimeout(() => {
              this.setState({
                pops1: false
              })
            }, 5000);
          })
          .catch((err) => {
            console.log({ err });
            this.setState({
              pops2: true,
            });
          });
      } catch (error) {
        this.setState((prevState) => ({
          transactionErrors: {
            ...prevState.transactionErrors,
            messageError: "Something went wrong!",
          },
        }));
        this.setState({
          pops2: true,
        });
      }
    } else {
      this.setState((prevState) => ({
        transactionErrors: {
          ...prevState.transactionErrors,
          messageError: "Something went wrong!",
        },
      }));
      this.setState({
        pops2: true,
      });
    }
  };

  earnAmount = async (event) => {
    event.preventDefault();

    if (this.state.selectedMonthAddress != null && this.state.amount != null) {
      try {
        var prov = new ethers.providers.Web3Provider(this.state.provider);

        var signer = await prov.getSigner(0);

        var contractInit = new ethers.Contract(
          this.state.selectedMonthAddress,
          singleOppurtunity,
          signer
        );

        var amo = await this.toFixed(this.state.amount * 10 ** 18);

        var con = contractInit.joinOpportunity(amo.toString());

        con
          .then(async (data) => {
            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: data.hash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                // gasUsed: data.gasUsed,
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops4: true,
            });

            var set = await prov.waitForTransaction(data.hash);

            
            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: set.transactionHash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                gasUsed: set.gasUsed.toString(),
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops1: true,
              pops4: false,
              index: null
            }, () => {
              this.startingFunctions();
            });
            
          })
          .catch((err) => {
            console.log({ err });
            this.setState({
              pops2: true,
            });
          });
      } catch (error) {
        this.setState((prevState) => ({
          transactionErrors: {
            ...prevState.transactionErrors,
            messageError: "Something went wrong!",
          },
        }));
        this.setState({
          pops2: true,
        });
      }
    } else {
      this.setState((prevState) => ({
        transactionErrors: {
          ...prevState.transactionErrors,
          messageError: "Something went wrong!",
        },
      }));
      this.setState({
        pops2: true,
      });
    }
  };

  getNFTBalance = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();
      this.setState({
        nftBalance: balance,
      });
    }
  };

  tokenIdData = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();

      var tokenIdArray = [];

      for (let i = 0; i < balance; i++) {
        var tokenId = await contractInit.methods
          .tokenOfOwnerByIndex(this.state.account, i)
          .call();

        var singleContractAddress = await contractInit.methods
          .getNftParent(tokenId)
          .call();

        tokenIdArray.push({
          tokenId,
          singleContractAddress,
        });
      }

      this.setState(
        {
          tokenIdDetails: tokenIdArray,
        },
        () => {
          this.tokenHistory();
        }
      );
    }
  };

  convertDate = async (data) => {
    var da = new Date(data * 1000);
    var month = (await parseInt(da.getMonth())) + 1;
    return da.getDate() + "-" + month + "-" + da.getFullYear();
  };

  tokenHistory = async () => {
    var history = [];

    if (this.state.tokenIdDetails.length > 0) {
      for (let i = 0; i < this.state.tokenIdDetails.length; i++) {
        const element = this.state.tokenIdDetails[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element.singleContractAddress
        );

        var singleAssetName = await contractInit.methods.getName().call();

        var status = await singleAssetName
          .toString()
          .toUpperCase()
          .includes(Data.AssetData[1].toString().toUpperCase());

        if (status) {
          var tokenDetails = await contractInit.methods
            .gettokenDetails(element.tokenId)
            .call();

          var contractInit1 = new this.state.web3.eth.Contract(
            ERC20Token,
            tokenDetails.baseToken
          );

          var symbol = await contractInit1.methods.symbol().call();

          var dat = await this.convertDate(tokenDetails.startTimestamp);
          var enddat = await this.convertDate(tokenDetails.endTimestamp);

          history.push({
            tokenAddress: tokenDetails.baseToken,
            symbol,
            amount: tokenDetails.tokenAmount,
            startTime: dat,
            endTime: tokenDetails.endTimestamp,
            tokenId: tokenDetails.nfttokenId,
            apy: tokenDetails.apy,
          });
        }
      }

      this.setState({
        tokenHistory: history,
      });
    }
  };

  onchangeTokenId = (e) => {
    e.preventDefault();

    this.setState({
      formTokenId: e.target.value,
    });
  };

  withdrawAmount = async (id, name) => {
    var obj = await this.state.tokenIdDetails.find(
      ({ tokenId }) => tokenId === id
    );

    this.setState({
      popupToken: name,
    });

    var prov = new ethers.providers.Web3Provider(this.state.provider);

    var signer = await prov.getSigner(0);

    var contractInit = new ethers.Contract(
      obj.singleContractAddress,
      singleOppurtunity,
      signer
    );

    var con = contractInit.exitOpportunity(id);

    con
      .then(async (data) => {
        this.setState((prevState) => ({
          transactionDetails: {
            ...prevState.transactionDetails,
            hash: data.hash,
            // ETA: 1,
            gasLimit: data.gasLimit.toString(),
            // gasUsed: data.gasUsed,
            gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
          },
        }));
        this.setState({
          pops4: true,
        });

        var set = await prov.waitForTransaction(data.hash);

        
        this.setState((prevState) => ({
          transactionDetails: {
            ...prevState.transactionDetails,
            hash: set.transactionHash,
            // ETA: 1,
            gasLimit: data.gasLimit.toString(),
            gasUsed: set.gasUsed.toString(),
            gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
          },
        }));
        this.setState({
          pops1: true,
          pops4: false,
          index: null
        });
        this.startingFunctions();
      })
      .catch((err) => {
        console.log({ err });
        this.setState({
          pops2: true,
        });
      });
  };

  popup_energy = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div")
        .classList.remove("overlay_flight_traveldil_show");
  };

  closePopup1 = () => {
    this.setState({ pops1: false });
  };

  closePopup2 = () => {
    this.setState({ pops2: false });
  };

  closePopup4 = () => {
    this.setState({ pops4: false });
  };

  render() {
    const {
      oppurtunitiesTokens,
      index,
      amount,
      nftBalance,
      showAllowance,
      tokenIdDetails,
      tokenHistory,
      formTokenId,
      tokenRadioTokenIndex,
      monthRadioIndex,
      pops,
      message,
      Images,
      monthToken,
      usdValueList,
      web3,
      selectedMonthValue,
      ImagesPrice,
      tokenBalance,
      apyData,
      isLoading,
      apyText,
      apyPercentage,
      orderCardsArray,
      pops1,
      pops2,
      transactionErrors,
      priceDetailsArray,
      pops4,
      transactionDetails,
      popupToken,
    } = this.state;
    return (
      // <>
      //   {isLoading ? (
      //     <Loader />
      //   ) : (
      <>
        {pops1 ? (
          <Completed
            popupToken={popupToken}
            popup={pops1}
            transactionDetails={transactionDetails}
            closePopup={() => this.closePopup1()}
          />
        ) : (
          ""
        )}

        {pops2 ? (
          <Failed
            popupToken={popupToken}
            popup={pops2}
            transactionErrors={transactionErrors}
            closePopup={() => this.closePopup2()}
          />
        ) : (
          ""
        )}
        {pops4 ? (
          <Loading
            popupToken={popupToken}
            popup={pops4}
            transactionDetails={transactionDetails}
            closePopup={() => this.closePopup4()}
          />
        ) : (
          ""
        )}
        <div className="container-fluid">
          <div className="row ecofriend-div">
            {
              <Header
                propData={null}
                onChangeWeb3={(web3, provider) =>
                  this.onChangeWeb3(web3, provider)
                }
              />
            }
            {pops ? <Popup message={message} /> : <></>}
            {/* ------------------------- */}
            <div id="show_div" className="overlay_flight_traveldil">
              <div className="popup_flight_travlDil">
                <div className="popup">
                  <div className="search-box2">
                    <p
                      className="popup-close"
                      onClick={(e) => this.popup_energy(e, "cross")}
                    >
                      X
                    </p>
                    <div
                      className="coins-duoss"
                      style={{ marginLeft: "3.5rem" }}
                    >
                      <div className="img-coin">
                        <img
                          src="/eco.png"
                          alt=""
                          className="image-small-coin"
                        />
                      </div>
                    </div>
                    <div>
                      <input
                        type="text"
                        name=""
                        id=""
                        placeholder="Enter Id"
                        className="input-popup"
                        onChange={(e) => this.onchangeTokenId(e)}
                      />
                    </div>
                    <div>
                      <button
                        className="approves"
                        onClick={async (e) => {
                          e.preventDefault();

                          await this.withdrawAmount(formTokenId);
                        }}
                      >
                        WITHDRAW
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* --------------------------- */}

            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div className="all-coins">{priceDetailsArray}</div>
            </div>

            <div className="col-xxl-9 col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12">
              <div className="main-box">
                <div className="row">
                  <div className="col-xxl-7 col-xl-7 col-lg-7 col-md-12 col-sm-12 col-xs-12">
                    <div className="row add-apy">
                      {/* {oppurtunitiesTokens.map((data, i) => {
                        return <OrderCards data={data} i={i} />;
                      })} */}
                      {orderCardsArray}
                    </div>
                  </div>

                  <div className="col-xxl-5 col-xl-5 col-lg-5 col-md-12 col-sm-12 col-xs-12">
                    <div className="row">
                      <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="side-box">
                          <img src="/money.svg" alt="" className="dollar-img" />
                          <p className="token-choose">
                            Choose Token :{" "}
                            {monthToken
                              ? monthToken.toString().toUpperCase()
                              : ""}
                          </p>
                        </div>
                      </div>
                      <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="side-box">
                          <img
                            src="/duration-dark.svg"
                            alt=""
                            className="dollar-img"
                          />
                          <p className="token-choose">
                            Yield Period : {selectedMonthValue}{" "}
                          </p>
                        </div>
                      </div>
                      <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div className="side-box">
                          <img
                            src="/profit-dark.svg"
                            alt=""
                            className="dollar-img"
                          />
                          <p className="token-choose">
                            {" "}
                            {parseInt(apyData) / 100}%{" "}
                          </p>
                        </div>
                      </div>

                      <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <p className="amnt-clr">Yield Period</p>
                      </div>
                      <div className="col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div className="all-date" style={{ display: "block" }}>
                          {index != null ? (
                            <>
                              {oppurtunitiesTokens[index].month.length > 0 ? (
                                <>
                                  {oppurtunitiesTokens[index].month.map(
                                    (data, i) => {
                                      return (
                                        <div
                                          key={i}
                                          style={{
                                            width: "fit-content",
                                            float: "left",
                                          }}
                                        >
                                          <input
                                            class="checkbox-tools-month"
                                            type="radio"
                                            name="tools-month"
                                            id="tool-1"
                                            checked={monthRadioIndex == i}
                                          ></input>
                                          <div
                                            className="date-box"
                                            tabIndex="1"
                                            key={i}
                                            onClick={(e) =>
                                              this.selectedMonth(
                                                e,
                                                oppurtunitiesTokens[index]
                                                  .singleOppurtunityAddress[i],
                                                i,
                                                data,
                                                index
                                              )
                                            }
                                          >
                                            <p> {data} </p>
                                            <p className="months">MONTHS</p>
                                          </div>
                                        </div>
                                      );
                                    }
                                  )}
                                </>
                              ) : (
                                <div></div>
                              )}
                            </>
                          ) : (
                            <div></div>
                          )}
                        </div>
                      </div>
                      <br></br>

                      <div className="col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <p className="amnt-clr" style={{ marginTop: "1rem" }}>
                          Amount : {parseFloat(tokenBalance).toFixed(2)}{" "}
                        </p>
                      </div>

                      <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <input
                          type="number"
                          onWheel={(e) => e.target.blur()}
                          placeholder="Enter Amount"
                          className="number-input"
                          onChange={(e) => this.amountChange(e)}
                          style={{ marginTop: "1rem" }}
                        />
                      </div>

                      <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <button
                          className="earn-now"
                          onClick={(e) => this.allowance(e)}
                          style={{
                            display: showAllowance ? "block" : "none",
                            fontSize: "1rem",
                            marginTop: "1rem",
                          }}
                        >
                          APPROVE
                        </button>
                      </div>
                      <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <button
                          className="earn-now"
                          onClick={(e) => this.earnAmount(e)}
                          style={{
                            display: !showAllowance ? "block" : "none",
                            fontSize: "1rem",
                            marginTop: "1rem",
                          }}
                        >
                          EARN NOW
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12">
              <div className="main-box">
                <div className="row">
                  <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="/banner-1.png" alt="" className="banner-img" />
                  </div>
                  <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src={Banner2} alt="" className="banner-img" />
                  </div>
                  <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <img src="/banner-3.png" alt="" className="banner-img" />
                  </div>
                </div>
              </div>
            </div>

            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div className="my-orders-table">
                <p className="my-orders-head">My Orders</p>
              </div>
            </div>

            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              {/* <OrdersTable /> */}

              <div className="table-responsive">
                <table class="table orders-table-data">
                  <thead>
                    <tr>
                      <th scope="col" className="orders-th-data">
                        Token Id
                      </th>
                      <th scope="col" className="orders-th-datas">
                        Token Symbol
                      </th>
                      <th scope="col" className="orders-th-datas">
                        Amount
                      </th>
                      <th scope="col" className="orders-th-datas">
                        Added Date
                      </th>
                      <th scope="col" className="orders-th-datas">
                        Apy
                      </th>
                      <th scope="col" className="orders-th-data2">
                        Action
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {tokenHistory.length > 0 ? (
                      <>
                        {tokenHistory.map((data, i) => {
                          return (
                            <tr key={i} style={{ textAlign: "center" }}>
                              <th scope="row"> {data.tokenId} </th>
                              <td> {data.symbol} </td>
                              <td>{parseInt(data.amount) / 10 ** 18}</td>
                              <td> {data.startTime} </td>
                              <td> {parseInt(data.apy) / 100}% </td>
                              <td>
                                <button
                                  className="remove-btn-history"
                                  onClick={(e) =>
                                    this.withdrawAmount(
                                      data.tokenId,
                                      data.symbol
                                    )
                                  }
                                  disabled
                                  style={{ background: "grey" }}
                                >
                                  Remove
                                </button>
                              </td>
                            </tr>
                          );
                        })}
                      </>
                    ) : (
                      <></>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </>
      //   )}
      // </>
    );
  }
}

export default withRouter(Orders);
