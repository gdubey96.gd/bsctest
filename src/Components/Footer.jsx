import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "../Styles/footer.css";

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        <div className="container-fluid">
          <div className="row footer-row">
            <div className="lower-row">
            <div
                className="col footer-col"
                style={{ zIndex: "4" }}
              >
                <NavLink to="/referral">
                  <span>
                    <div className="footer5 footers">
                      <p className="footer-p">
                      <span>
                          <img src="/real-wallet.png" width="31.476" height="31.513" alt=""/>
                        </span>
                        <span style={{marginLeft: '10px'}}>Wallet </span>
                      </p>
                    </div>
                  </span>
                </NavLink>
              </div>
              <div
                className="col footer-col"
                style={{ zIndex: "4" }}
              >
                <NavLink to="/ecocelerator">
                  <span>
                    <div className="footer1 footers">
                      <p className="footer-p">
                        <span>
                          <img src="/eco.svg" width="31.476" height="31.513" alt=""/>
                        </span>
                        <span style={{marginLeft: '10px'}}>Ecocelerator</span>
                      </p>
                    </div>
                  </span>
                </NavLink>
              </div>
              <div
                className="col footer-col"
                style={{ zIndex: "3" }}
              >
                <NavLink to="/earn">
                  <div className="footer2 footers">
                    <p className="footer-p">
                      <span>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width="31.69"
                          height="37.813"
                          viewBox="0 0 41.69 47.813"
                        >
                          <g data-name="Group 32">
                            <g data-name="Group 31">
                              <path
                                data-name="Path 6"
                                d="M40.892 41.437l-2.881-3.841V25.742a17.183 17.183 0 00-10.83-15.94l3.335-6.411a1.226 1.226 0 00-.425-1.594l-1.226-.787a6.5 6.5 0 00-6.278-.376 4.047 4.047 0 01-3.477 0 6.5 6.5 0 00-6.279.376l-1.226.787a1.226 1.226 0 00-.425 1.594l3.331 6.411a17.183 17.183 0 00-10.83 15.94v11.853L.8 41.437a3.985 3.985 0 003.188 6.376H37.7a3.984 3.984 0 003.188-6.376zM13.867 3.253l.283-.181a4.027 4.027 0 013.893-.232 6.52 6.52 0 005.6 0 4.024 4.024 0 013.892.232l.284.181-3.016 5.8a16.606 16.606 0 00-7.923 0zm24.76 41.8a1.53 1.53 0 01-.923.308H3.985a1.533 1.533 0 01-1.226-2.452l3.127-4.169a1.226 1.226 0 00.245-.736V25.742a14.713 14.713 0 0129.427 0v12.261a1.226 1.226 0 00.245.736l3.127 4.169a1.531 1.531 0 01-.303 2.144z"
                                fill="#fff"
                              />
                            </g>
                          </g>
                          <g data-name="Group 34">
                            <g data-name="Group 33">
                              <path
                                data-name="Path 7"
                                d="M23.814 28.922L18.972 26.5a1.047 1.047 0 01.467-1.984h2.81a1.05 1.05 0 011.047 1.047v1.405h2.452v-1.405a3.5 3.5 0 00-3.5-3.5h-.179v-2.452h-2.452v2.452h-.177a3.5 3.5 0 00-1.565 6.63l4.842 2.422a1.047 1.047 0 01-.467 1.984h-2.81a1.05 1.05 0 01-1.047-1.047v-.179H15.94v.179a3.5 3.5 0 003.5 3.5h.179v2.452h2.452v-2.452h.179a3.5 3.5 0 001.565-6.63z"
                                fill="#fff"
                              />
                            </g>
                          </g>
                        </svg>
                      </span>{" "}
                      Earn
                    </p>
                  </div>
                </NavLink>
              </div>
              <div
                className="col footer-col"
                style={{ zIndex: "2" }}
              >
                <NavLink to="/treasury">
                  <div className="footer3 footers">
                    <p className="footer-p">
                      <span>
                        <img
                          style={{ height: "1.5rem" }}
                          src="/arrowpng.png"
                          alt=""
                        />
                      </span>{" "}
                      Treasury
                    </p>
                  </div>
                </NavLink>
              </div>
              <div
                className="col footer-col"
                style={{ zIndex: "1" }}
              >
                <NavLink to="/swap">
                  <div className="footer4 footers">
                    <p className="footer-p">
                      {" "}
                      <span>
                        <img
                          style={{ height: "1.5rem" }}
                          src="/swap.svg"
                          alt=""
                        />
                      </span>{" "}
                      Swap
                    </p>
                  </div>
                </NavLink>
              </div>


              <div
                className="col footer-col"
                style={{ zIndex: "1" }}
              >
                <NavLink to="/ecolizer">
                  <div className="footer6 footers">
                    <p className="footer-p">
                      {" "}
                      <span>
                        <img
                          style={{ height: "1.3rem" }}
                          src="/ecolizer.png"
                          alt=""
                        />
                      </span>{" "}
                      Ecolizer
                    </p>
                  </div>
                </NavLink>
              </div>

            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Footer;
