import React, { Component } from "react";

class MyOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
      const { tokenHistory } = this.props
    return (
      <>
        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div className="my-orders-table">
            <p className="my-orders-head">My Orders</p>
          </div>
        </div>

        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div className="table-responsive">
            <table class="table orders-table-data">
              <thead>
                <tr>
                  <th scope="col" className="orders-th-data">
                    Token Id
                  </th>
                  <th scope="col" className="orders-th-datas">
                    Token Symbol
                  </th>
                  <th scope="col" className="orders-th-datas">
                    Amount
                  </th>
                  <th scope="col" className="orders-th-datas">
                    Apy
                  </th>
                  <th scope="col" className="orders-th-data2">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                {tokenHistory.length > 0 ? (
                  <>
                    {tokenHistory.map((data, i) => {
                      return (
                        <>
                          <tr key={i} style={{ textAlign: "center" }}>
                            <th scope="row"> {data.tokenId} </th>
                            <td> {data.symbol} </td>
                            <td> {parseInt(data.amount) / 10 ** 18} </td>
                            <td> {parseInt(data.apy) / 100}% </td>
                            <td>
                              {" "}
                              {/* <button
                                className="remove-btn-history"
                                onClick={(e) =>
                                  this.withdrawAmount(e, data.tokenId)
                                }
                              >
                                Remove
                              </button> */}
                            </td>
                          </tr>
                        </>
                      );
                    })}
                  </>
                ) : (
                  <></>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  }
}

export default MyOrders;
