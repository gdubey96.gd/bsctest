import Web3 from "web3";
import WalletConnectProvider from "@walletconnect/web3-provider";

const setWeb3ForMetamask = async () => {
  var web3Provider = new Web3.providers.HttpProvider(
    "https://bsc-dataseed1.binance.org/"
  );

  var web3 = new Web3(web3Provider);

  return web3;
};

const setWeb3ForWallet = async () => {
  const web3Provider = new WalletConnectProvider({
    rpc: {
      56: "https://bsc-dataseed1.binance.org/",
    },
    chainId: 56,
    bridge: "https://bridge.walletconnect.org",
  });

  var web3 = new Web3(web3Provider);

  return web3;
};

export { setWeb3ForMetamask, setWeb3ForWallet }
