import React, { Component } from "react";
import Header from "./Header";
import Footer from "./Footer";
import "../Styles/ecofriend.css";
import Reward from "./Reward";
import HistoryPopup from "./HistoryPopup";

const URL = "https://ecocelium.io/api/";

class EcoFriend extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pops: false,
      pops2: false,
      ecoIp: "https://ecocelium.io/api/",
      getEcoAddress: "user/getEcoAddress",
      getMemoId: "user/getEthId", //memoid
      updateReferralLink: "user/updateReferralId",
      withdrawEarn: "user/earnWithdrawn",
      rewardDetail: "user/getRewardDetails",
      downlineLink: "user/getDownLineDetails",
      withdrawLink: "user/earnWithdrawn",
      generateMemoidLink: "user/generateEcoID",
      account: null,
      memoId: null,
      ethAddress: null,
      booster: 0,
      matchBonus: 0,
      rewardAmount: 0,
      total: 0,
      downline: [],
      referralId: null,
      omcQuat: null,
    };
  }

  onChangeWeb3 = (web3) => {
    this.setState(
      {
        web3: web3,
      },
      async () => {
        if (
          this.state.web3 != null &&
          Object.keys(this.state.web3).length != 0
        ) {
          var accounts = await this.state.web3.eth.getAccounts();
          this.setState({ account: accounts[0] }, () => {
            this.getRewardData();
          });
        }
      }
    );
  };

  closePopup = () => {
    this.setState({ pops: false });
  };

  closePopup2 = () => {
    this.setState({ pops2: false });
  };

  getRewardData = async () => {
    await this.generateMemoid();
    this.getMemoId();
    this.getDownline();
  };

  // Referral link =   // "https://ecocelium.io/wallet/?memoId=" + this.ECOMemo;

  generateMemoid = () => {
    fetch(this.state.ecoIp + this.state.generateMemoidLink, {
      method: "POST",
      headers: {
        "content-type": "application/json",
        accept: "application/json",
      },
      body: JSON.stringify({
        ethAddress: this.state.account.toLowerCase(),
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log("Generate Memo Id :::::", response);
      })
      .catch((err) => {
        alert("Something went wrong");
      });
  };

  addReferral = () => {
    fetch(this.state.ecoIp + "user/updateReferralId", {
      method: "POST",
      headers: {
        "content-type": "application/json",
        accept: "application/json",
      },
      body: JSON.stringify({
        memoid: this.state.memoId,
        referralId: this.state.ethAddress,
        reqEthAddress: this.state.account.toLowerCase(),
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        // response.message
        alert("Success");
      })
      .catch((err) => {
        console.log(err);
        alert("Something went wrong");
      });
  };

  getRewardDetails = () => {
    fetch(this.state.ecoIp + this.state.rewardDetail, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
      },
      body: JSON.stringify({
        ethAddress: this.state.ethAddress,
        memoid: this.state.memoId,
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        this.setState(
          {
            booster: response.data.booster,
            matchBonus: response.data.matchBonus,
            rewardAmount: response.data.rewardAmount,
            total:
              response.data.booster +
              response.data.matchBonus +
              response.data.rewardAmount,
          },
          () => {
            this.getOMCData();
          }
        );
      })
      .catch((err) => {
        console.log(err);
        alert("Something went wrong");
      });
  };

  getDownline = () => {
    fetch(this.state.ecoIp + this.state.downlineLink, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
      },
      body: JSON.stringify({
        ethAddress: this.state.account,
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        if (response.statusCode == 200) {
          this.setState({
            downline: response.data,
          });
        } else {
          console.log("Some error :::::", response);
        }
      })
      .catch((err) => {
        console.log(err);
        alert("Something went wrong");
      });
  };

  getMemoId = () => {
    fetch(
      this.state.ecoIp +
        this.state.getEcoAddress +
        "/?ethAddress=" +
        this.state.account,
      {
        method: "GET",
        headers: {
          "content-type": "application/json",
          accept: "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((response) => {
        console.log({ response });
        this.setState(
          {
            memoId: response.data.memoid,
            referralId: response.data.referralId,
          },
          () => {
            this.getReferralAddress();
          }
        );
      })
      .catch((err) => {
        console.log(err);
        alert("Something went wrong");
      });
  };

  getReferralAddress = () => {
    fetch(
      this.state.ecoIp + this.state.getMemoId + "/?memoid=" + this.state.memoId,
      {
        method: "POST",
        headers: {
          "content-type": "application/json",
          accept: "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((response) => {
        this.setState(
          {
            ethAddress: response.data.ethAddress,
          },
          () => {
            // this.addReferral()
            this.getRewardDetails();
          }
        );
      })
      .catch((err) => {
        console.log(err);
        alert("Something went wrong");
      });
  };

  withdraw = () => {
    if (this.state.total <= 10) {
      alert("Withdraw is not possible amount less than 10");
      return false;
    }

    fetch(this.state.ecoIp + this.state.withdrawLink, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        accept: "application/json",
      },
      body: JSON.stringify({
        memoid: this.state.memoId,
        ethAddress: this.state.referralId,
        rewardAmount: this.state.total,
        booster: this.state.booster,
        matchBonus: this.state.matchBonus,
      }),
    })
      .then((response) => response.json())
      .then((response) => {
        console.log("response :::::::::::::", response);
        alert("success");
      })
      .catch((err) => {
        console.log(err);
        alert("Something went wrong");
      });
  };

  getOMCData = () => {
    fetch(
      "https://api.coingecko.com/api/v3/simple/price?ids=ormeus-cash&vs_currencies=USD",
      {
        method: "GET",
        headers: {
          "content-type": "application/json",
          accept: "application/json",
        },
      }
    )
      .then((response) => response.json())
      .then((response) => {
        var price = response["ormeus-cash"].usd;

        var getAvailabelRewardUSD = this.state.total;
        this.setState({
          omcQuat: getAvailabelRewardUSD / price,
        });
      })
      .catch((err) => {
        console.log(err);
        alert("Something went wrong");
      });
  };

  render() {
    const {
      pops,
      pops2,
      booster,
      matchBonus,
      rewardAmount,
      total,
      downline,
      referralId,
      omcQuat,
      memoId,
    } = this.state;

    return (
      <>
        {
          <Header
            propData={null}
            onChangeWeb3={(web3) => this.onChangeWeb3(web3)}
            referralId={referralId}
          />
        }
        <div className="container-fluid" style={{ backgroundColor: "#f7f7f7" }}>
          {pops ? (
            <Reward
              popup={pops}
              omcQuat={omcQuat}
              closePopup={() => this.closePopup()}
              withdraw={() => this.withdraw()}
            />
          ) : (
            ""
          )}

          {pops2 ? (
            <HistoryPopup popup={pops2} closePopup={() => this.closePopup2()} />
          ) : (
            ""
          )}
          <div className="row ecofriend-div">
            <div className="col-xxl-2 col-xl-2 col-lg-2 col-md-12 col-sm-12 col-xs-12">
              <div className="ecofriend-history">
                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div className="profile-main">
                    <div
                      className="profile"
                      onClick={(e) => {
                        e.preventDefault();
                        navigator.clipboard.writeText(
                          "https://ecocelium.io/wallet/?memoId=" + memoId
                        );
                        alert("Copied");
                      }}
                    >
                      <img src="/addProfile.png" alt="" />
                      <p className="invite-msg">Invite an EcoFriend</p>
                    </div>
                  </div>
                </div>

                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div className="profile-main" style={{ marginTop: "0px" }}>
                    <div className="reload" style={{ marginTop: "0.5rem" }}>
                      <img src="/reload.png" alt="" />{" "}
                      <span
                        className="history"
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                          this.setState({
                            pops2: true,
                          });
                        }}
                      >
                        History
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
              {referralId == null ? (
                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div className="profiles">
                    <div className="request-data ">
                      <img src="/request.png" alt="" />
                      <input
                        type="text"
                        placeholder="Enter EcoFriend Code"
                        className="input-field-d"
                      />
                      <button className="add">Add</button>
                    </div>
                  </div>
                </div>
              ) : (
                <></>
              )}

              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="profiless" style={{ height: "4.3rem" }}>
                  <div className="request-data ">
                    <p className="ecofamily">My EcoFamily</p>
                  </div>
                  {downline.length > 0 ? (
                    <>
                      {downline.map((data) => {
                        return (
                          <>
                            <div className="request-data ">
                              <p
                                className="ecofamily"
                                style={{
                                  color: "#a09c9c",
                                }}
                              >
                                <span>Wallet Address </span>
                                <span>
                                  {data.ethAddress.substring(0, 4) +
                                    "..." +
                                    data.ethAddress.substring(
                                      data.ethAddress.length - 4
                                    )}
                                </span>
                              </p>
                            </div>
                            <hr
                              style={{
                                width: "inherit",
                                borderTop: "1px solid rgba(0, 0, 0, 0.1)",
                                marginTop: "0px",
                                marginBottom: "0px",
                              }}
                            />
                          </>
                        );
                      })}
                    </>
                  ) : (
                    <></>
                  )}
                </div>
              </div>

              {referralId != null ? (
                <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div className="profiless">
                    <div className="request-data ">
                      <p className="ecofamily">EcoFriend</p>
                    </div>
                    <div className="request-data ">
                      <p className="ecofamily" style={{ color: "#a09c9c" }}>
                        <span>Wallet Address - </span>
                        <span>
                          {referralId.substring(0, 4) +
                            "..." +
                            referralId.substring(referralId.length - 4)}
                        </span>
                      </p>
                    </div>
                    <hr
                      style={{
                        width: "inherit",
                        borderTop: "1px solid rgba(0, 0, 0, 0.1)",
                        marginTop: "0px",
                        marginBottom: "0px",
                      }}
                    />
                  </div>
                </div>
              ) : (
                <></>
              )}
            </div>
            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 fourthcol">
              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 insidefourthcol">
                <div className="profilesss">
                  <div className="request-data-reward">
                    <p className="reward-value">Referral Reward</p>
                    <p className="ecofamily">{rewardAmount} USD</p>
                  </div>
                </div>
              </div>

              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 insidefourthcol">
                <div className="profiless">
                  <div className="request-data-reward">
                    <p className="reward-value">Booster Reward</p>
                    <p className="ecofamily">{booster} USD</p>
                  </div>
                </div>
              </div>
              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 insidefourthcol">
                <div className="profiless">
                  <div className="request-data-reward">
                    <p className="reward-value">Matching Reward</p>
                    <p className="ecofamily">{matchBonus} USD</p>
                  </div>
                </div>
              </div>
              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 insidefourthcol">
                <div className="profiless">
                  <div className="request-data-reward">
                    <p className="reward-available">Available Reward</p>
                    <p className="ecofamily">{total} USD</p>
                  </div>
                </div>
              </div>

              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 insidefourthcol">
                <button
                  className="reward"
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    this.setState({
                      pops: true,
                    });
                  }}
                >
                  Remove Reward
                </button>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

export default EcoFriend;
