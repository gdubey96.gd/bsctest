import React, { Component } from 'react';
class Header2 extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <>
            <div className="container">
                <div className="row">

                <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="header-img">
                  <img src="/ecocelium.jpeg" alt="" className="order-img" />
                </div>
                </div>

                <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div className="date-box date-boxs">
                  <div className="gst-div">
                    <p>
                      <span>
                        <i class="fas fa-map-marker-alt address-icon"></i>
                      </span>{' '}
                      My Address
                    </p>
                    <p className="gst">0x5njkdrf8uhja7f7873</p>
                  </div>
                  <div className="gst-div">
                    <p>
                      <span>
                        <i class="fas fa-user-friends"></i>
                      </span>{' '}
                      My Referrer
                    </p>
                    <p className="gst">0x5njkdrf8uhja7f7873</p>
                  </div>

              </div>
            </div>

                </div>

            </div>
            </>
         );
    }
}
 
export default Header2;