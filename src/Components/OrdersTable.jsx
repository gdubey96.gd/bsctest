import React, { Component } from "react";
import singleOppurtunity from "../ABI/singleOppurtunity.json";
import Data from "../Constants/contract";
import ERC20Token from "../ABI/ERC20.json";
import oppurtunityNFTContract from "../ABI/oppurtunityNFTContract.json";

class OrdersTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tokenHistory: [],
      tokenIdDetails: [],
      web3: null,
      account: null,
      contractAddress: "0x7E1b859F6C792cABBc2d5581703a3ee1d09674Fd",
      isLoading: false,
      contractInit: null,
      oppurtunities: [],
      oppurtunitiesTokens: [],
      showMonths: false,
      monthToken: null,
      selectedMonthAddress: null,
      index: null,
      amount: 0,
      nftBalance: null,
      showAllowance: false,
      allowanceAmount: 0,
      tokenIdDetails: [],
      tokenHistory: [],
      formTokenId: null,
      tokenRadioTokenIndex: null,
      monthRadioIndex: null,
      pops: false,
      message: null,
      usdValueList: [],
      selectedMonthValue: null,
      tokenBalance: 0,
      apyData: 0,
      apyArray: [],
      apyText: true,
      apyPercentage: true,
    };
  }

  
  componentDidMount() {
    // this.fetchUsdValue();
    this.setState(
      {
        web3: this.props.state,
      },
      () => {
    this.tokenIdData();
      }
    );
  }

  tokenHistory = async () => {
    var history = [];

    if (this.state.tokenIdDetails.length > 0) {
      for (let i = 0; i < this.state.tokenIdDetails.length; i++) {
        const element = this.state.tokenIdDetails[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element.singleContractAddress
        );

        var singleAssetName = await contractInit.methods.getName().call();

        var status = await singleAssetName
          .toString()
          .toUpperCase()
          .includes(Data.AssetData[1].toString().toUpperCase());

        if (status) {
          var tokenDetails = await contractInit.methods
            .gettokenDetails(element.tokenId)
            .call();

          var contractInit1 = new this.state.web3.eth.Contract(
            ERC20Token,
            tokenDetails.baseToken
          );

          var symbol = await contractInit1.methods.symbol().call();

          var dat = await this.convertDate(tokenDetails.startTimestamp);
          var enddat = await this.convertDate(tokenDetails.endTimestamp);

          history.push({
            tokenAddress: tokenDetails.baseToken,
            symbol,
            amount: tokenDetails.tokenAmount,
            startTime: dat,
            endTime: tokenDetails.endTimestamp,
            tokenId: tokenDetails.nfttokenId,
            apy: tokenDetails.apy,
          });
        }
      }

      this.setState({
        tokenHistory: history,
      });
    }
  };

  tokenIdData = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();

      var tokenIdArray = [];

      for (let i = 0; i < balance; i++) {
        var tokenId = await contractInit.methods
          .tokenOfOwnerByIndex(this.state.account, i)
          .call();

        var singleContractAddress = await contractInit.methods
          .getNftParent(tokenId)
          .call();

        tokenIdArray.push({
          tokenId,
          singleContractAddress,
        });
      }

      this.setState({
        tokenIdDetails: tokenIdArray,
      });

      await this.tokenHistory();
    }
  };

  withdrawAmount = async (id) => {
    var obj = await this.state.tokenIdDetails.find(
      ({ tokenId }) => tokenId === id
    );

    var contractInit = new this.state.web3.eth.Contract(
      singleOppurtunity,
      obj.singleContractAddress
    );

    contractInit.methods
      .exitOpportunity(id)
      .send({ from: this.state.account })
      .on("transactionHash", (hash) => {
        this.setState({
          pops: true,
          message: hash,
        });
        setTimeout(() => {
          this.setState({
            pops: false,
          });
        }, 5000);
      })
      .on("receipt", (receipt) => {
        this.setState({
          pops: true,
          message: receipt.transactionHash,
        });
        setTimeout(() => {
          this.setState({
            pops: false,
          });
        }, 5000);

        this.startingFunctions();
      })
      .on("error", (error) => {
        this.setState({
          pops: true,
          message: error.message
            ? `Error - ${error.message}`
            : `Error - ${error.receipt.transactionHash}`,
        });
        setTimeout(() => {
          this.setState({
            pops: false,
          });
        }, 5000);
      });
  };

  render() {
      const { tokenHistory,tokenIdDetails ,web3 } = this.state;
    return (
      <>
        <div className="table-responsive">
          <table class="table orders-table-data">
            <thead>
              <tr>
                <th scope="col" className="orders-th-data">
                  Token Id
                </th>
                <th scope="col" className="orders-th-datas">
                  Token Symbol
                </th>
                <th scope="col" className="orders-th-datas">
                  Amount
                </th>
                <th scope="col" className="orders-th-datas">
                  Added Date
                </th>
                <th scope="col" className="orders-th-datas">
                  Apy
                </th>
                <th scope="col" className="orders-th-data2">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {tokenHistory.length > 0 ? (
                <>
                  {tokenHistory.map((data, i) => {
                    return (
                      <tr key={i} style={{ textAlign: "center" }}>
                        <th scope="row"> {data.tokenId} </th>
                        <td> {data.symbol} </td>
                        <td>{parseInt(data.amount) / 10 ** 18}</td>
                        <td> {data.startTime} </td>
                        <td> {parseInt(data.apy) / 100}% </td>
                        <td>
                          <button
                            className="remove-btn-history"
                            onClick={(e) => this.withdrawAmount(data.tokenId)}
                            disabled
                            style={{ background: "grey" }}
                          >
                            Remove
                          </button>
                        </td>
                      </tr>
                    );
                  })}
                </>
              ) : (
                <></>
              )}
            </tbody>
          </table>
        </div>
      </>
    );
  }
}

export default OrdersTable;
