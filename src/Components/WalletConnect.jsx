import React, { Component } from 'react';
import  "../Styles/walletconnect.css";

class WalletConnect extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <>
            <div className="container-fluid">
                <div className="row main-wallet">
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div className="wallet-connect">
                        <div className="wallet-connect2">
                            <div className="wallet-connect3">
                                    <img src="/wallet-icon-new.png" alt="" className="wallet-img"/>
                                    <p className="name-wallet">WalletConnect</p>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div className="metamask-connect">
                        <div className="metamask-connect2">
                            <div className="metamask-connect3">
                                    <img src="/metamask.png" alt="" className="metamask-img"/>
                                    <p className="name-wallet">Connect Metamask</p>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            </>
         );
    }
}
 
export default WalletConnect;