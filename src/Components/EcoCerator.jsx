import React, { Component } from "react";
import "../Styles/ecocerator.css";
import Header from "./Header";
// import { web3 } from "./Header";
import Footer from "./Footer";
import oppurtunityNFTContract from "../ABI/oppurtunityNFTContract.json";
import MasterOppurtunity from "../ABI/masterOppurtunity.json";
import singleOppurtunity from "../ABI/singleOppurtunity.json";
import Data from "../Constants/contract";
import ERC20Token from "../ABI/ERC20.json";
import { EcoceleratorTokens } from "../Constants/contract";
import { tokenEcoceleratorNames } from "../Constants/token";
import tokenData from "../Constants/token";
import WBTC from "../images/B.png";
import ECO from "../images/eco.png";
import ETH from "../images/ether.png";
import OMC from "../images/omc.png";
import ORME from "../images/ormeus.png";
import USDT from "../images/busd.svg";
import WBNB from "../images/binance.png";
import Popup from "./Popup";
import EcoToken from "./EcoToken";
import Completed from "./Completed";
import Failed from "./Failed";
import Loading from "./Loading";

import * as ethers from "ethers";

class EcoCerator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Images: [ECO, OMC, ORME, USDT, WBTC, ETH, WBNB],
      LPImages: [USDT, ECO, OMC, ORME],
      web3: null,
      account: null,
      contractAddress: "0x7E1b859F6C792cABBc2d5581703a3ee1d09674Fd",
      contractInit: null,
      isLoading: false,
      oppurtunities: [],
      oppurtunitiesTokens: [],
      tokenDetails: [],
      nftBalance: null,
      index: null,
      showAllowance: false,
      allowanceAmount: 0,
      tokenIdDetails: [],
      tokenHistory: [],
      formTokenId: null,
      showModal: false,
      modalShown: false,
      modalShown2: false,
      pops: false,
      message: null,
      amount: 0,
      popupIndex: 0,
      popupToken: null,
      pops4: false,
      pops1: false,
      pops2: false,
      provider: null,
      transactionDetails: {
        hash: null,
        ETA: null,
        gasLimit: null,
        gasUsed: null,
        gasPrice: null,
      },
      transactionErrors: {
        message: "Transaction Failed!",
        messageError: "Something went wrong! Please try again!",
      },
    };
  }

  Modal({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
        style={{ zIndex: "5" }}
      >
        <div
          className="modal-content"
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
          style={{ zIndex: "5", borderRadius: "30px" }}
        >
          <p
            style={{ textAlign: "right", fontWeight: "500", cursor: "pointer" }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  Modal2({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
        style={{ zIndex: "5" }}
      >
        <div
          className="modal-content"
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
          style={{ zIndex: "5", borderRadius: "30px" }}
        >
          <p
            style={{ textAlign: "right", fontWeight: "500", cursor: "pointer" }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  // componentDidMount() {
  //   if (Object.keys(this.props.state).length != 0) {
  //     this.setState(
  //       {
  //         web3: this.props.state,
  //       },
  //       () => {
  //         this.startingFunctions();
  //       }
  //     );
  //   }
  // }

  startingFunctions = async () => {
    this.setState({
      isLoading: true,
    });
    await this.getAccounts();
    await this.initializeContract();
    await this.getAllOppurtunities();
    await this.getAllTokenName();
    await this.getTokenBalance();
    await this.getNFTBalance();
    await this.tokenIdData();
    this.setState({
      isLoading: false,
    });
  };

  onChangeWeb3 = (web3, provider) => {
    this.setState(
      {
        web3: web3,
        provider: provider,
      },
      () => {
        this.startingFunctions();
      }
    );
  };

  convertDate = async (data) => {
    var da = new Date(data * 1000);
    var month = (await parseInt(da.getMonth())) + 1;
    return da.getDate() + "-" + month + "-" + da.getFullYear();
  };

  getAccounts = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var network = await this.state.web3.eth.net.getNetworkType();
      var accounts = await this.state.web3.eth.getAccounts();
      // this.setState({ account: "0x21a29a50F66d5E8DB6E1F700d3987e81C21f259c" });
      this.setState({ account: accounts[0] });
      localStorage.setItem("address", accounts[0]);
      // this.setState({
      //   account: "0x0ee5f030090a6388db1d28f812690c6cc0484359",
      // });
    }
  };

  initializeContract = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      // await this.getAccounts();
      var contractInit = new this.state.web3.eth.Contract(
        MasterOppurtunity,
        Data.EcoceleratorMasterOpportunityContract
      );
      this.setState({ contractInit });
    }
  };

  getAllOppurtunities = async () => {
    if (this.state.contractInit != null) {
      var router = await this.state.contractInit.methods
        .getActiveOpportunities()
        .call();
      this.setState({ oppurtunities: router }, () => {
        console.log("oppurtunities :::::::::::", this.state.oppurtunities);
      });
    }
  };

  getAllTokenName = async () => {
    if (this.state.oppurtunities.length > 0) {
      var allTokens = [];

      for (let i = 0; i < this.state.oppurtunities.length; i++) {
        const element = this.state.oppurtunities[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element
        );

        var name = await contractInit.methods.getName().call();

        var tokenName = await this.checkTokenName(name);
        var address = await contractInit.methods.getTokenAddress().call();
        var nftAddress = await contractInit.methods.getNFTAddress().call();
        var apy = await contractInit.methods.getApy().call();

        var obj = {
          name,
          originalName: [],
          address: [],
          singleOppurtunityAddress: [],
          nftAddress: [],
          apy: [],
        };

        obj.name = tokenName;
        obj.originalName.push(name);
        obj.address.push(address);
        obj.singleOppurtunityAddress.push(element);
        obj.nftAddress.push(nftAddress);
        obj.apy.push(apy);

        allTokens.push(obj);
      }

      this.setState({ oppurtunitiesTokens: allTokens }, () => {});
    }
  };

  tokenExists = (arr, name) => {
    return arr.some(function (el) {
      return el.name === name;
    });
  };

  checkTokenName = async (data) => {
    var tokenName;
    for (let i = 0; i < tokenData.tokenEcoceleratorNames.length; i++) {
      const element = tokenData.tokenEcoceleratorNames[i];
      var value = await data
        .toString()
        .toUpperCase()
        .includes(element.toUpperCase());
      if (value) {
        tokenName = element;
      }
    }
    return tokenName;
  };

  getTokenBalance = async () => {
    var tokenArray = [];

    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      for (let i = 0; i < EcoceleratorTokens.length; i++) {
        var element = EcoceleratorTokens[i];

        var contractInit = new this.state.web3.eth.Contract(
          ERC20Token,
          element
        );

        var decimal = await contractInit.methods.decimals().call();
        console.log({ decimal });

        var balance = await contractInit.methods
          .balanceOf(this.state.account)
          .call();

        var symbol = await contractInit.methods.symbol().call();

        tokenArray.push({
          decimal,
          balance: balance / 10 ** decimal,
          symbol,
        });
      }
      this.setState(
        {
          tokenDetails: tokenArray,
        },
        () => {}
      );

      await this.tokenIdData();
    }
  };

  getNFTBalance = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();

      this.setState({
        nftBalance: balance,
      });
    }
  };

  amountChange = async (event, index) => {
    event.preventDefault();
    this.setState({
      amount: event.target.value,
    });

    await this.checkAllowance(index);

    if (
      this.state.allowanceAmount < this.state.amount ||
      this.state.amount == 0
    ) {
      this.setState({
        showAllowance: true,
      });
    } else {
      this.setState({
        showAllowance: false,
      });
    }

    await this.checkAllowance(index);
  };

  checkAllowance = async (index) => {
    var contractInit = new this.state.web3.eth.Contract(
      ERC20Token,
      EcoceleratorTokens[index]
    );

    if (index >= 1) {
      var decimal = await contractInit.methods.decimals().call();
      var amount = await contractInit.methods
        .allowance(
          this.state.account,
          Data.LPTokens[index == 1 ? 0 : index == 2 ? 1 : index == 3 ? 2 : ""]
        )
        .call();

      this.setState({
        allowanceAmount: amount / 10 ** decimal,
      });
    } else {
      var decimal = await contractInit.methods.decimals().call();
      var amount = await contractInit.methods
        .allowance(
          this.state.account,
          this.state.oppurtunitiesTokens[index].singleOppurtunityAddress[0]
        )
        .call();

      this.setState({
        allowanceAmount: amount / 10 ** decimal,
      });
    }
  };

  toFixed = (x) => {
    if (Math.abs(x) < 1.0) {
      var e = parseInt(x.toString().split("e-")[1]);
      if (e) {
        x *= Math.pow(10, e - 1);
        x = "0." + new Array(e).join("0") + x.toString().substring(2);
      }
    } else {
      var e = parseInt(x.toString().split("+")[1]);
      if (e > 20) {
        e -= 20;
        x /= Math.pow(10, e);
        x += new Array(e + 1).join("0");
      }
    }
    return x;
  };

  allowance = async (event, index) => {
    event.preventDefault();

    if (this.state.amount != 0) {
      try {
        var prov = new ethers.providers.Web3Provider(this.state.provider);

        var signer = await prov.getSigner(0);

        var contractInit = new ethers.Contract(
          EcoceleratorTokens[index],
          ERC20Token,
          signer
        );

        var amo = await this.toFixed(10 ** 71);

        var singleOpportunity =
          index >= 1
            ? Data.LPTokens[
                index == 1 ? 0 : index == 2 ? 1 : index == 3 ? 2 : ""
              ]
            : this.state.oppurtunitiesTokens[this.state.popupIndex]
                .singleOppurtunityAddress[0];

        this.setState({
          modalShown: false,
          showAllowance: false,
        });

        var con = contractInit.approve(singleOpportunity, amo.toString());

        con
          .then(async (data) => {
            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: data.hash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                // gasUsed: data.gasUsed,
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops4: true,
            });

            var set = await prov.waitForTransaction(data.hash);

            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: set.transactionHash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                gasUsed: set.gasUsed.toString(),
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops1: true,
              pops4: false,
            });
          })
          .catch((err) => {
            console.log({ err });
            this.setState({
              pops2: true,
            });
          });
      } catch (error) {
        this.setState((prevState) => ({
          transactionErrors: {
            ...prevState.transactionErrors,
            messageError: "Something went wrong!",
          },
        }));
        this.setState({
          pops2: true,
        });
      }
    } else {
      this.setState((prevState) => ({
        transactionErrors: {
          ...prevState.transactionErrors,
          messageError: "Something went wrong!",
        },
      }));
      this.setState({
        pops2: true,
      });
    }
  };

  earnAmount = async (event) => {
    event.preventDefault();

    if (this.state.amount != 0) {
      try {
        var prov = new ethers.providers.Web3Provider(this.state.provider);

        var signer = await prov.getSigner(0);

        if (this.state.popupIndex >= 1) {
          var contractInit = new ethers.Contract(
            Data.LPTokens[
              this.state.popupIndex == 1
                ? 0
                : this.state.popupIndex == 2
                ? 1
                : this.state.popupIndex == 3
                ? 2
                : ""
            ],
            singleOppurtunity,
            signer
          );
        } else {
          var contractInit = new ethers.Contract(
            this.state.oppurtunitiesTokens[
              this.state.popupIndex
            ].singleOppurtunityAddress[0],
            singleOppurtunity,
            signer
          );
        }

        var amo = await this.toFixed(this.state.amount * 10 ** 18);

        this.setState({
          modalShown: false,
          showAllowance: false,
        });

        var con = contractInit.joinOpportunity(amo.toString());

        con
          .then(async (data) => {
            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: data.hash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                // gasUsed: data.gasUsed,
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops4: true,
            });

            var set = await prov.waitForTransaction(data.hash);

            this.startingFunctions();

            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: set.transactionHash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                gasUsed: set.gasUsed.toString(),
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops1: true,
              pops4: false,
            });
          })
          .catch((err) => {
            console.log({ err });
            this.setState({
              pops2: true,
            });
          });
      } catch (error) {
        this.setState((prevState) => ({
          transactionErrors: {
            ...prevState.transactionErrors,
            messageError: "Something went wrong!",
          },
        }));
        this.setState({
          pops2: true,
        });
      }
    } else {
      this.setState((prevState) => ({
        transactionErrors: {
          ...prevState.transactionErrors,
          messageError: "Something went wrong!",
        },
      }));
      this.setState({
        pops2: true,
      });
    }
  };

  tokenIdData = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();

      var tokenIdArray = [];

      for (let i = 0; i < balance; i++) {
        var tokenId = await contractInit.methods
          .tokenOfOwnerByIndex(this.state.account, i)
          .call();

        var singleContractAddress = await contractInit.methods
          .getNftParent(tokenId)
          .call();

        tokenIdArray.push({
          tokenId,
          singleContractAddress,
        });
      }

      this.setState(
        {
          tokenIdDetails: tokenIdArray,
        },
        () => {
          this.tokenHistory();
        }
      );
    }
  };

  tokenHistory = async () => {
    var history = [];

    var lpToken = [
      {
        name: "lp-eco-busd",
        address: "0xAbf23a5C1018b209dD83F423C76154cfbE9Ef116",
      },
      {
        name: "lp-omc-busd",
        address: "0x1Ea2c1c021100eE61a543EEb0cE4847D11500ed9",
      },
      {
        name: "lp-orme-busd",
        address: "0x243723239917da8B9a525031F15a9fd7F1f6745D",
      },
    ];

    if (this.state.tokenIdDetails.length > 0) {
      for (let i = 0; i < this.state.tokenIdDetails.length; i++) {
        const element = this.state.tokenIdDetails[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element.singleContractAddress
        );

        var singleAssetName = await contractInit.methods.getName().call();

        var status = await singleAssetName
          .toString()
          .toUpperCase()
          .includes(Data.AssetData[2].toString().toUpperCase());

        if (status) {
          var tokenDetails = await contractInit.methods
            .gettokenDetails(element.tokenId)
            .call();

          var contractInit1 = new this.state.web3.eth.Contract(
            ERC20Token,
            tokenDetails.baseToken
          );

          var symbol = await contractInit1.methods.symbol().call();

          var dat = await this.convertDate(tokenDetails.startTimestamp);

          var lpName;

          if (symbol == "Cake-LP") {
            lpName = await lpToken.find(
              (o) => o.address == element.singleContractAddress
            ).name;
          }

          history.push({
            tokenAddress: tokenDetails.baseToken,
            symbol: symbol == "Cake-LP" ? lpName.toUpperCase() : symbol,
            amount: tokenDetails.tokenAmount,
            startTime: dat,
            endTime: tokenDetails.endTimestamp,
            tokenId: tokenDetails.nfttokenId,
            apy: tokenDetails.apy,
          });
        }
      }

      this.setState({
        tokenHistory: history,
      });
    }
  };

  onchangeTokenId = (e) => {
    e.preventDefault();

    this.setState({
      formTokenId: e.target.value,
    });
  };

  withdrawAmount = async (id) => {
    console.log({id});
    console.log('this.state.popupIndex :::::::::::', this.state.popupIndex);
    var prov = new ethers.providers.Web3Provider(this.state.provider);

    var signer = await prov.getSigner(0);
    
    if (this.state.popupIndex > 1) {

      var contractInit = new ethers.Contract(
        Data.LPTokens[
          this.state.popupIndex == 1
            ? 0
            : this.state.popupIndex == 2
            ? 1
            : this.state.popupIndex == 3
            ? 2
            : ""
        ],
        singleOppurtunity,
        signer
      );
    } else {
      var contractInit = new ethers.Contract(
        this.state.oppurtunitiesTokens[
          this.state.popupIndex
        ].singleOppurtunityAddress[0],
        singleOppurtunity,
        signer
      );
    }

    this.setState({
      modalShown2: false,
    });

    var con = contractInit.exitOpportunity(id);

    con
      .then(async (data) => {
        this.setState((prevState) => ({
          transactionDetails: {
            ...prevState.transactionDetails,
            hash: data.hash,
            // ETA: 1,
            gasLimit: data.gasLimit.toString(),
            // gasUsed: data.gasUsed,
            gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
          },
        }));
        this.setState({
          pops4: true,
        });

        var set = await prov.waitForTransaction(data.hash);

        this.startingFunctions();

        this.setState((prevState) => ({
          transactionDetails: {
            ...prevState.transactionDetails,
            hash: set.transactionHash,
            // ETA: 1,
            gasLimit: data.gasLimit.toString(),
            gasUsed: set.gasUsed.toString(),
            gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
          },
        }));
        this.setState({
          pops1: true,
          pops4: false,
        });
      })
      .catch((err) => {
        console.log({ err });
        this.setState({
          pops2: true,
        });
      });
  };

  closePopup1 = () => {
    this.setState({ pops1: false });
  };

  closePopup2 = () => {
    this.setState({ pops2: false });
  };

  closePopup4 = () => {
    this.setState({ pops4: false });
  };

  render() {
    const {
      tokenDetails,
      nftBalance,
      index,
      Images,
      tokenHistory,
      oppurtunitiesTokens,
      showAllowance,
      tokenIdDetails,
      formTokenId,
      modalShown,
      modalShown2,
      popupIndex,
      popupToken,
      pops,
      message,
      web3,
      LPImages,
      isLoading,
      pops4,
      pops1,
      pops2,
      transactionErrors,
      transactionDetails,
    } = this.state;
    return (
      <>
        {
          <Header
            propData={web3}
            onChangeWeb3={(web3, provider) => this.onChangeWeb3(web3, provider)}
          />
        }
        {pops ? <Popup message={message} /> : <></>}
        {pops1 ? (
          <Completed
            popupToken={"busd"}
            popup={pops1}
            transactionDetails={transactionDetails}
            closePopup={() => this.closePopup1()}
          />
        ) : (
          ""
        )}

        {pops2 ? (
          <Failed
            popupToken={"busd"}
            popup={pops2}
            transactionErrors={transactionErrors}
            closePopup={() => this.closePopup2()}
          />
        ) : (
          ""
        )}
        {pops4 ? (
          <Loading
            popupToken={"busd"}
            popup={pops4}
            transactionDetails={transactionDetails}
            closePopup={() => this.closePopup4()}
          />
        ) : (
          ""
        )}
        <div className="container">
          <div className="row eco-friendly">
            <this.Modal
              shown={modalShown}
              close={() => {
                this.setState({
                  modalShown: false,
                });
              }}
            >
              <div className="coins-duoss">
                <div className="img-coin">
                  {popupIndex < 1 ? (
                    <img src={USDT} alt="" className="image-small-coins" />
                  ) : (
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <img
                        src={LPImages[popupIndex]}
                        alt=""
                        className="image-small-coins"
                        style={{ zIndex: "2" }}
                      />
                      <img
                        src={USDT}
                        alt=""
                        className="image-small-coins"
                        style={{
                          zIndex: "3",
                          marginLeft: "-36%",
                          marginTop: "1.3rem",
                        }}
                      />
                    </div>
                  )}
                </div>
              </div>
              <div style={{ textAlign: "center" }}>
                <input
                  type="text"
                  name=""
                  id=""
                  placeholder="Enter Amount"
                  className="input-popup"
                  onChange={(e) => this.amountChange(e, popupIndex)}
                  style={{ boxShadow: "0px 5px 6px lightgrey" }}
                />
                {tokenDetails.length !== 0 ? (
                  <p>Balance : {parseFloat(tokenDetails[popupIndex].balance).toFixed(3)} </p>
                ) : (
                  <p>Balance : 0</p>
                )}
              </div>
              <div
                style={{
                  textAlign: "center",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <button
                  className="approve"
                  style={{
                    display: showAllowance ? "block" : "none",
                  }}
                  onClick={(e) => this.allowance(e, popupIndex)}
                >
                  APPROVE
                </button>
              </div>
              <div
                style={{
                  textAlign: "center",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <button
                  className="approve"
                  style={{
                    display: !showAllowance ? "block" : "none",
                  }}
                  onClick={(e) => this.earnAmount(e)}
                >
                  ADD
                </button>
              </div>
            </this.Modal>
            <this.Modal2
              shown={modalShown2}
              close={() => {
                this.setState({
                  modalShown2: false,
                });
              }}
            >
              <div
                className="coins-duoss"
                // style={{ height: popupIndex < 2 ? "auto" : "6rem" }}
              >
                <div
                  className="img-coin"
                  // style={{ width: popupIndex < 2 ? "auto" : "6.5rem" }}
                >
                  {popupIndex < 1 ? (
                    <img src={USDT} alt="" className="image-small-coins" />
                  ) : (
                    <div style={{ display: "flex", justifyContent: "center" }}>
                      <img
                        src={LPImages[popupIndex]}
                        alt=""
                        className="image-small-coins"
                        style={{ zIndex: "2" }}
                      />
                      <img
                        src={USDT}
                        alt=""
                        className="image-small-coins"
                        style={{
                          zIndex: "3",
                          marginLeft: "-36%",
                          marginTop: "1.3rem",
                        }}
                      />
                    </div>
                  )}
                </div>
                {/* <div
                  className={
                    popupIndex < 2
                      ? "coin-details"
                      : "coin-details coin-details2"
                  }
                >
                  <p>{popupToken}</p>
                </div> */}
              </div>
              <div>
                <input
                  type="text"
                  name=""
                  id=""
                  placeholder="Enter Id"
                  className="input-popup"
                  onChange={(e) => this.onchangeTokenId(e)}
                  style={{ boxShadow: "0px 5px 6px lightgrey" }}
                />
              </div>

              <div className="coins-duoss">
                <div className="img-coin">
                  <button
                    className="remove"
                    onClick={async (e) => {
                      e.preventDefault();

                      await this.withdrawAmount(formTokenId);
                    }}
                  >
                    Remove
                  </button>
                </div>
              </div>
            </this.Modal2>
            {/* ----------------------------------------------- */}
            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div className="main-eco-div">
                <div className="row">
                  {tokenDetails.length > 0 ? (
                    <>
                      {tokenDetails.map((data, i) => {
                        return (
                          <>
                            {i < 4 ? (
                              <>
                                <div
                                  className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12"
                                  key={i}
                                >
                                  <div className="row eco-div">
                                    <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                                      <div
                                        className="img-coin"
                                        style={{
                                          width: i < 1 ? "auto" : "6.5rem",
                                        }}
                                      >
                                        {i < 1 ? (
                                          <img
                                            src={i == 0 ? USDT : Images[i]}
                                            alt=""
                                            className="image-small-coins"
                                          />
                                        ) : (
                                          <div style={{ display: "flex" }}>
                                            <img
                                              src={LPImages[i]}
                                              alt=""
                                              className="image-small-coins"
                                              style={{
                                                zIndex: "0",
                                                height: "3rem",
                                                width: "3rem",
                                              }}
                                            />
                                            <img
                                              src={USDT}
                                              alt=""
                                              className="image-small-coins"
                                              style={{
                                                zIndex: "1",
                                                marginLeft: "-36%",
                                                marginTop: "1.3rem",
                                                height: "3rem",
                                                width: "3rem",
                                              }}
                                            />
                                          </div>
                                        )}
                                      </div>
                                    </div>
                                    <div
                                      className="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8"
                                      style={{
                                        lineHeight: "10px",
                                        marginLeft: i < 2 ? "" : "-1rem",
                                      }}
                                    >
                                      <p className="text-head">
                                        {tokenEcoceleratorNames[i]
                                          .toString()
                                          .toUpperCase()}
                                      </p>
                                      <p className="text-center">
                                        <span className="eco-heading-span">
                                          APY:
                                        </span>
                                        <span className="eco-data-span">
                                          {parseInt(
                                            oppurtunitiesTokens[i].apy
                                          ) / 100}
                                          %
                                        </span>
                                      </p>
                                      <p
                                        className="text-center"
                                        style={{ textAlign: "left" }}
                                      >
                                        <span className="eco-heading-span">
                                          Minimum Period:{" "}
                                        </span>{" "}
                                        <br />{" "}
                                        <span className="eco-data-span">
                                          7 Days
                                        </span>
                                      </p>
                                      <p className="text-center eco-heading-span">
                                        Your Balance:
                                      </p>
                                      <p>
                                        {parseFloat(data.balance).toFixed(2)}{" "}
                                      </p>
                                    </div>
                                    <div
                                      className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                                      style={{ textAlign: "center" }}
                                    >
                                      <button
                                        className="approve"
                                        onClick={(e) => {
                                          e.preventDefault();
                                          this.setState({
                                            modalShown: !this.state.modalShown,
                                            popupIndex: i,
                                            popupToken: data.symbol,
                                          });
                                        }}
                                      >
                                        ADD
                                      </button>
                                      <button
                                        className="remove"
                                        onClick={(e) => {
                                          e.preventDefault();
                                          this.setState({
                                            modalShown2:
                                              !this.state.modalShown2,
                                            popupIndex: i,
                                            popupToken: data.symbol,
                                          });
                                        }}
                                      >
                                        REMOVE
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </>
                            ) : (
                              <></>
                            )}
                          </>
                        );
                      })}
                    </>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            </div>
            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div className="my-orders-table">
                <p className="my-orders-head">My Orders</p>
              </div>
            </div>
            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div className="table-responsive">
                <table class="table orders-table-data">
                  <thead>
                    <tr>
                      <th scope="col" className="orders-th-data">
                        Token Id
                      </th>
                      <th scope="col" className="orders-th-datas">
                        Token Symbol
                      </th>
                      <th scope="col" className="orders-th-datas">
                        Amount
                      </th>
                      <th scope="col" className="orders-th-datas">
                        Added Date
                      </th>
                      <th scope="col" className="orders-th-data2">
                        Apy
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {tokenHistory.length > 0 ? (
                      <>
                        {tokenHistory.map((data, i) => {
                          return (
                            <tr key={i} style={{ textAlign: "center" }}>
                              <th scope="row"> {data.tokenId} </th>
                              <td> {data.symbol} </td>
                              <td> {parseInt(data.amount) / 10 ** 18} </td>
                              <td> {data.startTime} </td>
                              <td> {parseInt(data.apy) / 100}% </td>
                            </tr>
                          );
                        })}
                      </>
                    ) : (
                      <></>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

export default EcoCerator;
