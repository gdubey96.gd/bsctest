import React, { Component } from "react";
import { ImagesDetails } from "../Constants/token";
import "../Styles/ecocerator.css";
import "../Styles/completed.css";

class Loading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pops4: false,
      transactionHash: null,
      trans: null,
    };
  }

  async componentDidMount() {
    var { transactionDetails } = this.props;
    var acc = await transactionDetails.hash.substring(0, 4);
    var acc1 = await transactionDetails.hash.substring(
      transactionDetails.hash.length - 4
    );

    this.setState({
      transactionHash: transactionDetails.hash,
      trans: acc + "......" + acc1,
    });
  }

  Modal2({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
      >
        <div
          className="modal-content"
          style={{
            background: "#f2f2f2",
            borderRadius: "22px",
            height: "22.5rem",
            width: "25rem",
            // padding: "3rem",
            margin: "1rem  ",
          }}
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
        >
          <p
            style={{
              textAlign: "right",
              fontWeight: "500",
              cursor: "pointer",
              marginBottom: "0px",
            }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  render() {
    const { transactionHash, trans } = this.state;
    const { popup, closePopup, transactionDetails, popupToken } = this.props;

    return (
      <this.Modal2
        shown={popup}
        close={() => {
          closePopup();
        }}
      >
        <div className="">
          <p className="trans-complete">Your Transaction is in Progress!</p>
          <div className="eco-div eco-div-tick">
            <div
              style={{
                display: "flex",
                padding: "0.2rem",
                position: "relative",
              }}
            >
              <div className="uchka-hua-popup"></div>
              <div style={{ zIndex: "3" }}>
                <img
                  src={
                    ImagesDetails.find(
                      (o) => o.name.toUpperCase() == popupToken.toUpperCase()
                    ).image
                  }
                  alt=""
                  className="action-popup"
                />
                <div></div>
              </div>
              <div
                style={{
                  lineHeight: "0.3rem",
                  alignItems: "baseline",
                  marginTop: "1rem",
                  marginLeft: "1rem",
                }}
              >
                <p> {popupToken.toUpperCase()} </p>
                {/* <p>25.2831</p> */}
              </div>
            </div>
            <div>
              <img src="/spinner.svg" alt="" style={{ height: "3rem" }} />
            </div>
          </div>
          <div className="eco-div eco-div-list">
            <div className="completed">
              <p>Tx Hash:</p>
              <p
                onClick={() =>
                  window.open(
                    `https://bscscan.com/tx/${transactionHash}`,
                    "_blank"
                  )
                }
                style={{ cursor: "pointer" }}
              >
                {trans}{" "}
              </p>
            </div>
            <div className="completed">
              <p>ETA:</p>
              <p>2 min</p>
            </div>
            <div className="completed">
              <p>GAS Limit:</p>
              <p> 538,953 </p>
            </div>
            <div className="completed">
              <p>GAS Used:</p>
              <p> 359,169 </p>
            </div>
            <div className="completed">
              <p>GAS Price:</p>
              <p>10 Gwel</p>
            </div>
            {/* <div className="completed">
              <p>Free Spent:</p>
              <p>N/A ETH</p>
            </div> */}
          </div>
        </div>
      </this.Modal2>
    );
  }
}

export default Loading;
