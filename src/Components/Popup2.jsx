import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import  "../Styles/popup2.css";

class Popup2 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  popup_energy = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div")
        .classList.remove("overlay_flight_traveldil_show");
  };
  render() {
    return (
      <>
        <div
          id="show_div"
          className="overlay_flight_traveldil overlay_flight_traveldil_show"
        >
          <div className="popup_flight_travlDil">
            <div className="popup" style={{marginTop:'4rem'}}>
              <div className="search-box2" style={{background:'white',maxWidth:'40rem' , width:'100%'}}>
                  <NavLink to="/ecocelerator">
                <p  
                  className="popup-close"
                //   onClick={(e) => this.popup_energy(e, "cross")}
                >
                  X
                </p>
                </NavLink>
                {/* <div className="coins-duoss"> */}
                  <div className="coin-details" style={{ lineHeight: "1.5rem" }}>
                    <h4 className="beta-head">BETA TESTING DISCLAIMER </h4>
                    {/* style={{ wordBreak: "break-word" , whiteSpace:'pre-line' }} */}
                    <p >
                      Be aware this is a beta version of the EcoCelium platform.
                    </p>
                    <p>
                      {" "}
                      Please note that we are still undergoing final testing
                      before the official release.
                    </p>
                    <p>
                      EcoCelium will not be liable for any loss, whether such
                      loss is direct,
                    </p>
                    <p>
                      indirect, special or consequential, suffered by any party
                      as a result
                    </p>
                    <p> of their use of the EcoCelium platform.</p>
                    <p>
                      {" "}
                      Any activity done on this platform is done at the user's
                      own risk.
                    </p>
                    <p>
                      If you encounter any glitches in functionality or notice
                      other errors on the website, please inform us immediately
                      so whether can make the appropriate corrections. Your help
                      is greatly appreciated!
                    </p>
                    <p>Please email us at <span><a href="" className="support-link"> support@ecocelium.io</a></span> </p>
                  </div>
                {/* </div> */}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Popup2;
