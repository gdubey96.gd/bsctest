import Header from "./Header";
import React, { Component } from "react";
import "../Styles/swap.css";
import Footer from "./Footer";

class Swap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      web3: null,
      radioToggle: {
        data: null,
        toggle: false
      }
    };
  }

  componentDidMount() {
    this.setState({
      web3: this.props.state,
    });
  }

  onChangeWeb3 = (web3) => {
    this.setState(
      {
        web3: web3,
      },
      () => {
        // this.startingFunctions();
      }
    );
  };

  handleRadioClick = async(name) => {
    this.setState(prevState => {
      let radioToggle = Object.assign({}, prevState.radioToggle);
      radioToggle.data = name;
      return { radioToggle };
    })
  }

  popup_energy = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div")
        .classList.remove("overlay_flight_traveldil_show");
  };

  popup_energy2 = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div2")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div2")
        .classList.remove("overlay_flight_traveldil_show");
  };

  popup_energy3 = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div3")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div3")
        .classList.remove("overlay_flight_traveldil_show");
  };

  popup_energy4 = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div4")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div4")
        .classList.remove("overlay_flight_traveldil_show");
  };

  popup_energy5 = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div5")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div5")
        .classList.remove("overlay_flight_traveldil_show");
  };

  render() {
    const { web3, radioToggle } = this.state;
    return (
      <>
        {<Header
            propData={null}
            onChangeWeb3={(web3) => this.onChangeWeb3(web3)}
          />}
        <div className="container">
          <div className="row ecofriend-div swap">
            {/* ------------------------- */}
            <div
              id="show_div"
              className="overlay_flight_traveldil"
              style={{ overflowY: "scroll" }}
            >
              <div className="popup_flight_travlDil">
                <div className="popup">
                  <div
                    className="search-box2"
                    style={{
                      maxWidth: "45rem",
                      background: "white",
                      color: "black",
                    }}
                  >
                    <p
                      className="popup-close"
                      onClick={(e) => this.popup_energy(e, "cross")}
                    >
                      X
                    </p>
                    <p>
                      To swap your ERC20 Tokens to BEP20 tokens, copy the wallet
                      address and send the amount you want to swap. You can also
                      scan the QR with your Trust Wallet app. Please click the
                      “Swap Now” button to see the address and the QR code.
                    </p>

                    <p>
                      You will automatically receive your new BEP20 swapped
                      tokens in your wallet after completing the swap.
                    </p>

                    <p>
                      Click "Swap Now" below to see the address and the QR code
                    </p>

                    <button
                      className="swap-popup-btn"
                      onClick={(e) => this.popup_energy2(e, "add")}
                    >
                      <img
                        src="/swap.svg"
                        alt=""
                        className="swap-small"
                        style={{ cursor: "pointer" }}
                      />
                      <span> SWAP NOW</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            {/* --------------------------- */}

            {/* ------------------------- */}
            <div
              id="show_div2"
              className="overlay_flight_traveldil"
              style={{ overflowY: "scroll" }}
            >
              <div className="popup_flight_travlDil">
                <div className="popup">
                  <div
                    className="search-box2"
                    style={{
                      maxWidth: "45rem",
                      background: "white",
                      color: "black",
                    }}
                  >
                    <p
                      className="popup-close"
                      onClick={(e) => this.popup_energy2(e, "cross")}
                    >
                      X
                    </p>
                    <div style={{ height: "auto", width: "100%" }}>
                      <img src="/barcode2.png" alt="" className="barcode-img" />
                    </div>
                    <p style={{ wordBreak: "break-all" }}>
                      Address - 0xD5338807A621dd84C8945d729EC569f1f8397D7E{" "}
                      <span>
                        <i
                          class="fa fa-copy"
                          style={{ marginLeft: "1rem", cursor: "pointer" }}
                          onClick={(e) => {
                            e.preventDefault();
                            navigator.clipboard.writeText(
                              "0xD5338807A621dd84C8945d729EC569f1f8397D7E"
                            );
                            alert("Copied");
                          }}
                        ></i>
                      </span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            {/* --------------------------- */}

            {/* ------------------------- */}
            <div
              id="show_div3"
              className="overlay_flight_traveldil"
              style={{ overflowY: "scroll" }}
            >
              <div className="popup_flight_travlDil">
                <div className="popup" style={{marginTop:'0.5rem'}}>
                  <div
                    className="search-box2"
                    style={{
                      maxWidth: "45rem",
                      background: "white",
                      color: "black",
                    }}
                  >
                    <p
                      className="popup-close"
                      onClick={(e) => this.popup_energy3(e, "cross")}
                    >
                      X
                    </p>
                    <div style={{ height: "auto", width: "100%" }}>
                      <img
                        src="/warning.svg"
                        alt=""
                        style={{ height: "6rem" }}
                      />
                    </div>
                    <p className="bold-char">
                      Both a <span className="memo-heading">MEMO</span> and an
                      Address are required to sucessfully deposit your ECO
                      tokens to EcoCelium.
                    </p>
                    <p className="bold-char">
                      Transaction fee for Swap up to 20 million tokens - 1%
                    </p>
                    <p className="bold-char">
                      Transaction fee for Swap over 20 million tokens - 0.3%
                    </p>
                    <p>
                      To use the SWAP function successfully, you will need to
                      enter the wallet address for the respected tokens you are
                      swapping and your account{" "}
                      <span className="memo-heading">"MEMO."</span> (You can
                      find your <span className="memo-heading">MEMO </span>
                      code below the receiver wallet address).
                    </p>
                    <p>
                      Transfers with an incomplete or incorrect{" "}
                      <span className="memo-heading">MEMO</span> or Wallet
                      address are NOT refundable as both are needed to confirm
                      your profile's identity!
                    </p>
                    <p>
                      Once the BNB wallet address has received confirmations
                      validating the transaction, the Ethereum tx-id
                      (transaction ID) is created and listed on Etherscan.
                    </p>
                    You can see your transaction on the blockchain by searching
                    for the user Ethereum wallet address on Etherscan.
                    <p style={{ fontSize: "2rem", wordBreak:'break-word' }}>
                      After processing the transaction, please send confirmation
                      email to support@ecocelium.io
                    </p>
                    <button
                      className="swap-popup-btn"
                      onClick={(e) => this.popup_energy4(e, "add")}
                    >
                      <span>I Understand</span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
            {/* --------------------------- */}

            {/* ------------------------- */}
            <div
              id="show_div4"
              className="overlay_flight_traveldil"
              style={{ overflowY: "scroll" }}
            >
              <div className="popup_flight_travlDil">
                <div className="popup">
                  <div
                    className="search-box2"
                    style={{
                      maxWidth: "45rem",
                      background: "white",
                      color: "black",
                    }}
                  >
                    <p
                      className="popup-close"
                      onClick={(e) => this.popup_energy4(e, "cross")}
                    >
                      X
                    </p>
                    <p className="eco-view">ECO View</p>
                    <div style={{ height: "auto", width: "100%" }}>
                      <img src="/bscswapqrcode.png" alt="" className="barcode-img" />
                    </div>
                    <p style={{ wordBreak: "break-all" }}>
                      Address - bnb1js8pu8hdztcnlecagt9temu366rt2cv4qlf27r{" "}
                    </p>
                  </div>
                </div>
              </div>
            </div>
            {/* --------------------------- */}

            {/* ------------------------- */}
            <div
              id="show_div5"
              className="overlay_flight_traveldil"
              style={{ overflowY: "scroll" }}
            >
              <div className="popup_flight_travlDil">
                <div className="popup">
                  <div
                    className="search-box2"
                    style={{
                      maxWidth: "45rem",
                      background: "white",
                      color: "black",
                    }}
                  >
                    <p
                      className="popup-close"
                      onClick={(e) => this.popup_energy5(e, "cross")}
                    >
                      X
                    </p>
                    <p className="eco-view">Withdraw Eco Earning</p>
                    <div>
                      <input
                        type="text"
                        name=""
                        id=""
                        placeholder="Enter Receiver Address"
                        className="input-popup-data"
                      />
                    </div>
                    <div>
                      <input
                        type="text"
                        name=""
                        id=""
                        placeholder="Enter Amount"
                        className="input-popup-data"
                      />
                    </div>
                    <div>
                      <input
                        type="text"
                        name=""
                        id=""
                        placeholder="Enter Memo"
                        className="input-popup-data"
                      />
                    </div>
                    <p className="trans-text">
                      Transaction fee for Swap up to 20 million tokens - 1%
                    </p>
                    <p className="trans-text">
                      Transaction fee for Swap over 20 million tokens - 0.3%
                    </p>
                    <p style={{ fontSize: "2rem", wordBreak:'break-word' }}>
                      After processing the transaction, please send confirmation
                      email to support@ecocelium.io
                    </p>
                    <button className="submit-popup-btn">Submit</button>
                  </div>
                </div>
              </div>
            </div>
            {/* --------------------------- */}

            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
            <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <p className="swap-btn-txt2" style={{marginBottom:'0px' , textAlign:'center'}}>SWAP BEP-2 to ERC-20 TOKEN </p>
              <div className="swap-tokens swap-data">
              
                <input
                  class="checkbox-tools-swap"
                  type="radio"
                  name="tools"
                  id="tool-1"
                  checked={radioToggle.data == 'ERC20'}
                ></input>
                <div
                  className="erc20 erc20-data"
                  style={{
                    marginLeft: "3px",
                    margin: "0.4rem",
                    padding: "0.2rem",
                    display: "flex",
                    cursor: "pointer",
                    color: "white",
                    borderRadius: "18px",
                    background: "gray",
                  }}
                  // onClick={() => this.handleRadioClick('ERC20')}
                >
                  <img src="/eco.png" alt="" className="eco-small" />
                  <span className="token">ERC20</span>
                </div>

                <div className="erc20-arrow">
                  <p>
                    <i class="fas fa-caret-left arrow-left"></i>
                  </p>
                  <p>
                    <i class="fas fa-caret-right arrow-right"></i>
                  </p>
                </div>

                <input
                  class="checkbox-tools-swap"
                  type="radio"
                  name="tools"
                  id="tool-1"
                  // checked
                  checked={radioToggle.data == 'BEP2'}
                ></input>
                <div
                  className="erc20 erc20-data"
                  style={{
                    margin: "0.4rem",
                    padding: "0.2rem",
                    display: "flex",
                    cursor: "pointer",
                    cursor: "pointer",
                    // color: "white",
                    borderRadius: "18px",
                    // background: "lightgray",
                  }}
                  disabled
                  onClick={() => this.handleRadioClick('BEP2')}
                >
                  <img src="/eco.png" alt="" className="eco-small" />
                  <span className="token">BEP2</span>
                </div>

                <div
                  className="erc20-data-swapss"
                  style={{ cursor: "pointer" }}
                  onClick={(e) => radioToggle.data == 'ERC20' ? this.popup_energy5(e, "add") : this.popup_energy3(e, "add")}
                >
                  <img src="/swap.png" alt="" className="swap-pic" />
                  <p className="swap-txt" style={{marginBottom:'0px'}}>SWAP</p>
                </div>
              </div>
              <p className="swap-btn-text">SWAP ERC-20 TO BEP-20 TOKENS</p>
              <div className="swap-tokens" style={{ height: "12rem" }}>
                <div className="buttons-all">
                  <div className="btn-1" tabIndex="1">
                    <div className="erc20">
                      <img src="/eco.png" alt="" className="eco-small" />
                      <span className="token">ECO</span>
                    </div>
                  </div>

                  <div className="btn-1" tabIndex="1">
                    <div className="erc20">
                      <img src="/ormeus.png" alt="" className="eco-small" />
                      <span className="token">ORME</span>
                    </div>
                  </div>

                  <div className="btn-1" tabIndex="1">
                    <div className="erc20">
                      <img src="/omc.png" alt="" className="eco-small" />
                      <span className="token">OMC</span>
                    </div>
                  </div>
                </div>
                <div className="down-arrow" style={{ fontSize: "2rem" }}>
                  <div>
                    <i class="fas fa-caret-down" style={{ color: "blue" ,marginLeft:'1rem'}}></i>
                  </div>
                  <div>
                    <p style={{fontSize:'1.4rem'}}>
                      <span>
                        <img src="/binance.png" alt="" className="eco-small" />
                      </span>{" "}
                      BEP-20
                    </p>
                  </div>
                </div>
                <div style={{ height: "10rem", paddingTop: "0rem" }}  style={{ cursor: "pointer" }}
                      onClick={(e) => this.popup_energy(e, "add")}>
                  <div className="swap-curve">
                    <img
                      src="/swap.svg"
                      alt=""
                      className="swap-small"
                     
                    />
                    <p style={{textAlign:'center'}}>SWAP</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
        </div>
        <Footer />
      </>
    );
  }
}

export default Swap;
