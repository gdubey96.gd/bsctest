import React, { Component } from "react";
import { NavLink, Link, withRouter } from "react-router-dom";
import WalletConnectProvider from "@walletconnect/web3-provider";
import "../Styles/landingpage.css";
import Web3 from "web3";
import Completed from "./Completed";
import Newsletter from "./Newsletter";
import Failed from "./Failed";
import Loading from "./Loading";
import Info from "./Info";

// https://docs.google.com/spreadsheets/d/1p207tUoakB4l4SwHFmP_dlOgwxGicjN3FWpACjFft9w/edit#gid=0

class LandingPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ip: "https://bsc.ecocelium.io/api/",
      web3: null,
      account: null,
      contractAddress: "0x7E1b859F6C792cABBc2d5581703a3ee1d09674Fd",
      contractInit: null,
      pops: false,
      pops2: false,
      pops3: false,
      pops4: false,
      pops5: false,
      email: null,

    };
  }

  subscribe = () => {
    fetch(this.state.ip + "save", {
      method: "POST",
      headers: {
        "content-type": "application/json",
        accept: "application/json",
      },
      body: JSON.stringify({
        email: this.state.email
      }),
    })
      .then((response) => {
        console.log({response});
        return response.json()
      })
      .then((response) => {
        // response.message
        console.log(response);
        alert('Successfully subscribed')
      })
      .catch((err) => {
        console.log(err);
        alert('Something went wrong')
      });
  };

  handleChange = (data) => {
    this.setState({email: data})
  }

  componentDidMount() {
    // return window.location.assign('https://ecocelium.io/');

    localStorage.clear()
  }

  connectMetamask = async () => {
    var web3;

    if (window.ethereum) {
      web3 = new Web3(window.ethereum);

      if (window.ethereum.chainId == "0x38") {
        localStorage.setItem("wallet", "metamask");
        await this.props.setState({ ...web3 });
        this.props.history.push({
          pathname: "/popup",
        });
      } else {
        try {
          window.ethereum.enable().then(async () => {
            // User has allowed account access to DApp...
            if (window.ethereum.chainId == "0x38") {
              localStorage.setItem("wallet", "metamask");

              await this.props.setState({ ...web3 });
              this.props.history.push({
                pathname: "/popup",
              });
            } else {
              alert("Switch to BSC Mainet");
            }
          });
        } catch (e) {
          // User has denied account access to DApp...
          alert("User denied");
        }
      }
    }
    // Legacy DApp Browsers
    else if (window.web3) {
      web3 = new Web3(web3.currentProvider);

      this.setState({
        web3: web3,
      });

      this.props.setState({ ...web3 });

      // this.props.history.push({
      //   pathname: "/popup",
      // });
    }
    // Non-DApp Browsers
    else {
      alert("You have to install MetaMask !");
    }

    window.ethereum.enable();

    if (typeof web3 != "undefined") {
      this.web3Provider = web3.currentProvider;
      window.ethereum.enable();
    } else {
      this.web3Provider = new Web3.providers.HttpProvider(
        "https://bsc-dataseed1.binance.org/"
      );
      window.ethereum.enable();
    }
  };

  connectWalletProvider = async (e) => {
    e.preventDefault();

    const web3Provider = new WalletConnectProvider({
      rpc: {
        56: "https://bsc-dataseed1.binance.org/",
      },
      chainId: 56,
      bridge: "https://bridge.walletconnect.org",
    });

    var web3 = new Web3(web3Provider);

    localStorage.setItem("wallet", "trust");

    web3Provider
      .enable()
      .then((data) => {
        this.props.setState({ ...web3 });

        this.props.history.push({
          pathname: "/popup",
        });
      })
      .catch((err) => {
        console.log({ err });
      });

    this.setState({
      web3: web3,
    });
  };

  attachWalletProvider = async (e) => {
    e.preventDefault();

    let ethereum = window.ethereum;

    const data = [
      {
        chainId: "0x38",
        chainName: "Binance Smart Chain",
        nativeCurrency: {
          name: "BNB",
          symbol: "BNB",
          decimals: 18,
        },
        rpcUrls: ["https://bsc-dataseed.binance.org/"],
        blockExplorerUrls: ["https://bscscan.com/"],
      },
    ];

    try {
      var res = await ethereum.request({
        method: "wallet_addEthereumChain",
        params: data,
      });

      var web3 = new Web3(ethereum);

      this.props.setState({ ...web3 }, () => {
        this.props.history.push({
          pathname: "/popup",
        });
      });
    } catch (error) {
      console.log({ error });
    }

    /* eslint-disable */
    // ethereum
    //   .request({ method: "wallet_addEthereumChain", params: data })
  };

  closePopup2 = () => {
    this.setState({ pops: false });
  };

  closePopup3 = () => {
    this.setState({ pops3: false });
  };

  closePopup4 = () => {
    this.setState({ pops4: false });
  };

  closePopup5 = () => {
    this.setState({ pops5: false });
  };

  closePopup = () => {
    this.setState({ pops2: false });
  };

  render() {
    const { pops, pops2, pops3, pops4, pops5 } = this.state;
    return (
      <>
        <div className="landing-pages">
          <div className="container-fluid landing">
            {pops ? (
              <Newsletter popup={pops} closePopup={() => this.closePopup2()} handleChange={(data) => this.handleChange(data)} subscribe={() => this.subscribe()} />
            ) : (
              ""
            )}

            {pops2 ? (
              <Completed popup={pops2} closePopup={() => this.closePopup()} />
            ) : (
              ""
            )}

            {pops3 ? (
              <Failed popup={pops3} closePopup={() => this.closePopup3()} />
            ) : (
              ""
            )}

            {pops4 ? (
              <Loading popup={pops4} closePopup={() => this.closePopup4()} />
            ) : (
              ""
            )}

            {pops5 ? (
              <Info popup={pops5} closePopup={() => this.closePopup5()} />
            ) : (
              ""
            )}
            <div className="row landing-header-row">
              <div className="col top-header">
                <a href="https://ecocelium.org" style={{ width: "12rem" }}>
                  <div className="info-page top-header-p">
                    <p style={{ textAlign: "center" }}>
                      <span style={{ marginRight: "7px" }}>
                        <i class="fas fa-info-circle"></i>
                      </span>
                      Go to Info Page
                    </p>
                  </div>
                </a>
                <div
                  className="newsletter top-header-p"
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    this.setState({
                      pops: true,
                    });
                  }}
                >
                  <p style={{ textAlign: "center" }}>
                    <span style={{ marginRight: "7px" }}>
                      <i class="fas fa-envelope-open-text"></i>
                    </span>
                    Subscribe to our Newsletter
                  </p>
                </div>
              </div>
            </div>

            <div className="row wallet-row">
              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 para-col">
                <p className="welcome">Welcome to EcoCelium DeFi Platform!</p>
              </div>

              <div className="col-xxl-2 col-xl-2 col-lg-2 col-md-2 col-sm-12 col-12"></div>

              <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 wallet-col">
                <div className="wallet-connect">
                  <div className="wallet-connect2">
                    <div className="wallet-connect3">
                      <p className="launch">
                        Launch EcoCelium with <br />
                        Binance Smart Chain
                      </p>
                      <div className="row">
                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <div className="eth-data">
                            <div
                              style={{
                                display: "flex",
                                alignItems: "end",
                                justifyContent: "center",
                              }}
                            >
                              <div>
                                <img
                                  src="/binance.png"
                                  alt=""
                                  className="all-icom-img"
                                />
                              </div>
                              <div style={{ display: "grid" }}>
                                <img
                                  src="/info.png"
                                  alt=""
                                  style={{
                                    height: "1rem",
                                    margin: "0.3rem",
                                    cursor: "pointer",
                                  }}
                                  onClick={() => {
                                    this.setState({
                                      pops5: true,
                                    });
                                  }}
                                />
                                <img
                                  src="/add.png"
                                  alt=""
                                  style={{
                                    height: "1.5rem",
                                    margin: "0.1rem",
                                    cursor: "pointer",
                                  }}
                                  onClick={(e) => this.attachWalletProvider(e)}
                                />
                              </div>
                            </div>
                            <p className="bold-heading word-css">
                              Binance Smart Chain
                            </p>
                          </div>
                        </div>
                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-6 col-xs-6">
                              <div
                                className="wallet-underline"
                                onClick={(e) => this.connectMetamask(e)}
                                style={{
                                  cursor: "pointer",
                                  display: "flex",
                                }}
                              >
                                <div>
                                  <img
                                    src="/metamask.png"
                                    alt=""
                                    className="all-icom-imgs"
                                  />
                                  <div
                                    style={{
                                      display: "flex",
                                      alignItems: "baseline",
                                      justifyContent: "center",
                                    }}
                                  >
                                    <p
                                      className="small-head"
                                      style={{ letterSpacing: "2px" }}
                                    >
                                      METAMASK
                                    </p>
                                  </div>
                                </div>

                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    marginTop: "0.6rem",
                                  }}
                                >
                                  <p style={{ margin: "0.3rem" }}>
                                    <img
                                      src="/shutdown.png"
                                      alt=""
                                      style={{ height: "1.5rem" }}
                                    />
                                  </p>
                                </div>
                              </div>
                            </div>
                            <hr />
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-6 col-xs-6">
                              <div
                                className="wallet-underline"
                                onClick={(e) => this.connectWalletProvider(e)}
                                style={{
                                  cursor: "pointer",
                                  display: "flex",
                                  justifyContent: "space-between",
                                }}
                              >
                                <div>
                                  <img
                                    src="/wallet-icon-new.png"
                                    alt=""
                                    className="all-icom-imgs"
                                    style={{ width: "3rem" }}
                                  />
                                  <div
                                    style={{
                                      display: "flex",
                                      alignItems: "baseline",
                                      justifyContent: "center",
                                    }}
                                  >
                                    <p className="small-head">WalletConnect</p>
                                  </div>
                                </div>
                                <div>
                                  <p style={{ margin: "0.3rem" }}>
                                    <img
                                      src="/shutdown.png"
                                      alt=""
                                      style={{
                                        height: "1.5rem",
                                        visibility: "hidden",
                                      }}
                                    />
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12 wallet-col">
                <div className="metamask-connect">
                  <div className="metamask-connect2">
                    <div className="metamask-connect3">
                      <p className="launch">
                        Launch EcoCelium with
                        <br />
                        Ethereum Blockchain
                      </p>
                      <div className="row">
                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <div className="eth-data">
                            <img
                              src="/ether-real.png"
                              alt=""
                              className="all-icom-img"
                              style={{ width: "3rem" }}
                            />
                            <p className="bold-heading">Ethereum</p>
                          </div>
                        </div>
                        <div className="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                          <div className="row">
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-6 col-xs-6">
                              <a href="https://ecocelium.io/home/wallet">
                                <div
                                  className="wallet-underline"
                                  style={{
                                    cursor: "pointer",
                                    display: "flex",
                                  }}
                                >
                                  <div>
                                    <img
                                      src="/metamask.png"
                                      alt=""
                                      className="all-icom-imgs"
                                    />
                                    <div
                                      style={{
                                        display: "flex",
                                        alignItems: "baseline",
                                        justifyContent: "center",
                                      }}
                                    >
                                      <p
                                        className="small-head"
                                        style={{ letterSpacing: "2px" }}
                                      >
                                        METAMASK
                                      </p>
                                    </div>
                                  </div>

                                  <div
                                    style={{
                                      display: "flex",
                                      alignItems: "center",
                                      marginTop: "0.6rem",
                                    }}
                                  >
                                    <p style={{ margin: "0.3rem" }}>
                                      <img
                                        src="/shutdown.png"
                                        alt=""
                                        style={{ height: "1.5rem" }}
                                      />
                                    </p>
                                  </div>
                                </div>
                              </a>
                            </div>
                            <hr />
                            <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-6 col-xs-6">
                              <a href="https://ecocelium.io/home/wallet">
                                <div
                                  className="wallet-underline"
                                  style={{
                                    cursor: "pointer",
                                    display: "flex",
                                    justifyContent: "space-between",
                                  }}
                                >
                                  <div>
                                    <img
                                      src="/wallet-icon-new.png"
                                      alt=""
                                      className="all-icom-imgs"
                                      style={{ width: "3rem" }}
                                    />
                                    <div
                                      style={{
                                        display: "flex",
                                        alignItems: "baseline",
                                        justifyContent: "center",
                                      }}
                                    >
                                      <p className="small-head">
                                        WalletConnect
                                      </p>
                                    </div>
                                  </div>
                                  <div>
                                    <p style={{ margin: "0.3rem" }}>
                                      <img
                                        src="/shutdown.png"
                                        alt=""
                                        style={{
                                          height: "1.5rem",
                                          visibility: "hidden",
                                        }}
                                      />
                                    </p>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-xxl-2 col-xl-2 col-lg-2 col-md-2 col-sm-12 col-xs-12"></div>
            </div>

            <div className="row footer-rows">
              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="fixed-footer-data">
                  <div className="all-logo-footer">
                    <a
                      href={"https://www.facebook.com/EcoCeliumPlatform"}
                      className="footer-link"
                    >
                      <p className="logo-footer">
                        <i class="fab fa-facebook-f"></i>
                      </p>
                    </a>
                    <a
                      href="https://www.instagram.com/ecocelium_platform/"
                      className="footer-link"
                    >
                      <p className="logo-footer">
                        <i class="fab fa-instagram"></i>
                      </p>
                    </a>
                    <a
                      href="https://www.youtube.com/channel/UCupbnTrV-H6_MbZxgj7jIbQ"
                      className="footer-link"
                    >
                      <p className="logo-footer">
                        <i class="fab fa-youtube"></i>
                      </p>
                    </a>
                    <a href="https://t.me/ecocelium" className="footer-link">
                      <p className="logo-footer">
                        <i class="fab fa-telegram-plane"></i>
                      </p>
                    </a>
                  </div>
                  <div>
                    <p>All rights reserved by EcoCelium 2021.</p>
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(LandingPage);
