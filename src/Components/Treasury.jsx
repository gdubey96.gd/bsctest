import Header from "./Header";
import React, { Component } from "react";
import "../Styles/treasury.css";
import "../Styles/ecofriend.css";
import Footer from "./Footer";
import Header2 from "./Header2";
import oppurtunityNFTContract from "../ABI/oppurtunityNFTContract.json";
import MasterOppurtunity from "../ABI/masterOppurtunity.json";
import singleOppurtunity from "../ABI/singleOppurtunity.json";
import Data from "../Constants/contract";
import ERC20Token from "../ABI/ERC20.json";
import { Tokens } from "../Constants/contract";
import { tokenNames, ImagesDetails } from "../Constants/token";
import tokenData from "../Constants/token";
import WBTC from "../images/B.png";
import ECO from "../images/eco.png";
import ETH from "../images/ether.png";
import OMC from "../images/omc.png";
import ORME from "../images/ormeus.png";
import USDT from "../images/busd.svg";
import WBNB from "../images/binance.png";
import Popup from "./Popup";
import Loader from "./Loader";
import { withRouter } from "react-router";
import MyOrders from "./MyOrders";
import TreasuryTable from "./TreasuryTable";
import { setWeb3ForMetamask, setWeb3ForWallet } from "./Web3";
import Completed from "./Completed";
import Failed from "./Failed";
import Loading from "./Loading";

import * as ethers from "ethers";

class Treasury extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Images: [ECO, OMC, ORME, USDT, WBTC, ETH, WBNB],
      usdValueTokenList: [
        "ormeus-ecosystem",
        "ormeus-cash",
        "ormeuscoin",
        "binance-usd",
        "wrapped-bitcoin",
        "ethereum",
        "binancecoin",
      ],
      web3: null,
      isLoading: false,
      account: null,
      contractAddress: "0x7E1b859F6C792cABBc2d5581703a3ee1d09674Fd",
      contractInit: null,
      oppurtunities: [],
      oppurtunitiesTokens: [],
      tokenDetails: [],
      nftBalance: null,
      showAddRemove: false,
      showAddRemove2: false,
      showAddRemove3: false,
      index: null,
      amount: 0,
      showAllowance: false,
      allowanceAmount: 0,
      tokenIdDetails: [],
      tokenHistory: [],
      modalShown: false,
      formTokenId: null,
      pops: false,
      message: null,
      popupIndex: null,
      popupToken: null,
      usdValueList: [],
      pops4: false,
      pops1: false,
      pops2: false,
      trDataArray: [],
      provider: null,
      transactionDetails: {
        hash: null,
        ETA: null,
        gasLimit: null,
        gasUsed: null,
        gasPrice: null,
      },
      transactionErrors: {
        message: "Transaction Failed!",
        messageError: "Something went wrong! Please try again!",
      },
      addButton: true
    };
  }

  startingFunctions = async () => {
    this.setState({
      isLoading: true,
    });
    await this.fetchUsdValue();
    await this.getAccounts();
    await this.initializeContract();
    await this.getAllOppurtunities();
    await this.getAllTokenName();
    // await this.getTokenBalance();
    await this.getNFTBalance();
    this.tokenIdData();
    this.setState({
      isLoading: false,
      addButton: false
    });
  };

  onChangeWeb3 = (web3, provider) => {
    this.setState(
      {
        web3: web3,
        provider: provider,
      },
      () => {
        this.startingFunctions();
      }
    );
  };

  Modal({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
      >
        <div
          className="modal-content"
          style={{ borderRadius: "15px" }}
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
        >
          <p
            style={{ textAlign: "right", fontWeight: "500", cursor: "pointer" }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  getAccounts = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var network = await this.state.web3.eth.net.getNetworkType();
      var accounts = await this.state.web3.eth.getAccounts();
      // this.setState({ account: "0x21a29a50F66d5E8DB6E1F700d3987e81C21f259c" });
      this.setState({ account: accounts[0] });

      var network = await this.state.web3.eth.net.getNetworkType();
      var accounts = await this.state.web3.eth.getAccounts();
      // this.setState({ account: "0x21a29a50F66d5E8DB6E1F700d3987e81C21f259c" });
      this.setState({ account: accounts[0] });
      // this.setState({
      //   account: "0x0ee5f030090a6388db1d28f812690c6cc0484359",
      // });
    }
  };

  fetchUsdValue = async () => {
    var price = [];
    for (let i = 0; i < this.state.usdValueTokenList.length; i++) {
      const element = this.state.usdValueTokenList[i];

      var data = await this.getPrice(element);

      price.push(parseFloat(data).toFixed(4));
    }
    this.setState({
      usdValueList: price,
    });
  };

  getPrice = async (element) => {
    try {
      var priceData = await fetch(
        `https://api.coingecko.com/api/v3/simple/price?ids=${element}&vs_currencies=USD`
      );

      var jsonPrice = await priceData.json();

      var jsonData = jsonPrice[element] ? jsonPrice[element].usd : 0;

      return jsonData;
    } catch (error) {}
  };

  initializeContract = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        MasterOppurtunity,
        Data.TreasuryMasterOpportunityContract
      );
      this.setState({ contractInit });
    }
  };

  getAllOppurtunities = async () => {
    if (this.state.contractInit != null) {
      var router = await this.state.contractInit.methods
        .getActiveOpportunities()
        .call();
      this.setState({ oppurtunities: router });
    }
  };

  getAllTokenName = async () => {
    if (this.state.oppurtunities.length > 0) {
      var allTokens = [];
      var tokenArray = [];
      const { Images } = this.state;
      this.setState({trDataArray : []})
      for (let i = 0; i < this.state.oppurtunities.length; i++) {
        const element = this.state.oppurtunities[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element
        );

        var name = await contractInit.methods.getName().call();

        var tokenName = await this.checkTokenName(name);
        var address = await contractInit.methods.getTokenAddress().call();
        var nftAddress = await contractInit.methods.getNFTAddress().call();
        var apy = await contractInit.methods.getApy().call();

        var contractInit = new this.state.web3.eth.Contract(
          ERC20Token,
          Tokens[i]
        );

        var decimal = await contractInit.methods.decimals().call();

        var balance = await contractInit.methods
          .balanceOf(this.state.account)
          .call();
        var symbol = await contractInit.methods.symbol().call();

        var usdBalance = await parseFloat(
          (balance / 10 ** decimal) * parseFloat(this.state.usdValueList[i])
        ).toFixed(2)

        this.setState((prevState) => ({
          trDataArray: [
            ...prevState.trDataArray,
            <tr className="tr-data tr-content" key={i}>
              <td className="tr-data-head-after">
                <div className="tdha">
                  <img src={ImagesDetails.find(o => o.name == tokenNames[i]).image} alt="" className="table-img" />
                  <span style={{ cursor: "pointer" }}>{tokenNames[i].toUpperCase()}</span>
                </div>
              </td>
              <td className="tr-data-head-after">
                <span>{parseFloat(balance / 10 ** decimal).toFixed(2)}</span>
              </td>
              <td className="tr-data-head-after">
                <span> {usdBalance} </span>
              </td>
              <td className="tr-data-head-after">
                <span>{parseInt(apy) / 100}%</span>
              </td>
              <td className="tr-data-head-after-button">
                <button
                  className="remove-btn-history2 treasury-add-button"
                  style={{ padding: "0.5rem" }}
                  onClick={(e) => {
                    e.preventDefault();
                    this.setState({
                      modalShown: !this.state.modalShown,
                      popupIndex: i,
                      popupToken: tokenNames[i].toUpperCase(),
                    });
                  }}
                >
                  ADD
                </button>
              </td>
            </tr>,
          ],
        }));

        tokenArray.push({
          decimal,
          balance: balance / 10 ** decimal,
          symbol: tokenNames[i].toUpperCase(),
          USDAssetValue: usdBalance,
        });

        var obj = {
          name,
          originalName: [],
          address: [],
          singleOppurtunityAddress: [],
          nftAddress: [],
          apy: [],
        };

        obj.name = tokenName;
        obj.originalName.push(name);
        obj.address.push(address);
        obj.singleOppurtunityAddress.push(element);
        obj.nftAddress.push(nftAddress);
        obj.apy.push(apy);

        allTokens.push(obj);
      }

      this.setState({ oppurtunitiesTokens: allTokens });
      this.setState({
        tokenDetails: tokenArray,
      });
    }
  };

  // tokenExists = (arr, name) => {
  //   return arr.some(function (el) {
  //     return el.name === name;
  //   });
  // };

  checkTokenName = async (data) => {
    var tokenName;
    for (let i = 0; i < tokenData.tokenNames.length; i++) {
      const element = tokenData.tokenNames[i];
      var value = await data
        .toString()
        .toUpperCase()
        .includes(element.toUpperCase());
      if (value) {
        tokenName = element;
      }
    }
    return tokenName;
  };

  // getTokenBalance = async () => {
  //   if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
  //     var tokenArray = [];

  //     const { Images, oppurtunitiesTokens } = this.state;

  //     for (let i = 0; i < Tokens.length; i++) {
  //       const element = Tokens[i];

  //       var contractInit = new this.state.web3.eth.Contract(
  //         ERC20Token,
  //         element
  //       );

  //       var decimal = await contractInit.methods.decimals().call();

  //       var balance = await contractInit.methods
  //         .balanceOf(this.state.account)
  //         .call();
  //       var symbol = await contractInit.methods.symbol().call();

  //       var usdBalance = await parseFloat(
  //         (balance / 10 ** decimal) * parseFloat(this.state.usdValueList[i])
  //       ).toFixed(3);

  //       tokenArray.push({
  //         decimal,
  //         balance: balance / 10 ** decimal,
  //         symbol: tokenNames[i].toUpperCase(),
  //         USDAssetValue: usdBalance,
  //       });

  //       this.setState((prevState) => ({
  //         trDataArray: [
  //           ...prevState.trDataArray,
  //           <tr className="tr-data tr-content" key={i}>
  //             <td className="tr-data-head-after">
  //               <div className="tdha">
  //                 <img src={Images[i]} alt="" className="table-img" />
  //                 <span style={{ cursor: "pointer" }}>{symbol}</span>
  //               </div>
  //             </td>
  //             <td className="tr-data-head-after">
  //               <span>{parseFloat(balance / 10 ** decimal).toFixed(3)}</span>
  //             </td>
  //             <td className="tr-data-head-after">
  //               <span> {usdBalance} </span>
  //             </td>
  //             <td className="tr-data-head-after">
  //               <span>{parseInt(oppurtunitiesTokens[i].apy) / 100}%</span>
  //             </td>
  //             <td className="tr-data-head-after-button">
  //               <button
  //                 className="remove-btn-history2 treasury-add-button"
  //                 style={{ padding: "0.5rem" }}
  //                 onClick={(e) => {
  //                   e.preventDefault();
  //                   this.setState({
  //                     modalShown: !this.state.modalShown,
  //                     popupIndex: i,
  //                     popupToken: symbol,
  //                   });
  //                 }}
  //               >
  //                 ADD
  //               </button>
  //             </td>
  //           </tr>,
  //         ],
  //       }));
  //     }
  //     this.setState({
  //       tokenDetails: tokenArray,
  //     });
  //   }
  // };

  getNFTBalance = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      console.log('getNFTBalance --------------------------');
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();

      this.setState({
        nftBalance: balance,
      });
    }
  };

  openAddRemove = async (event, i) => {
    event.preventDefault();
    this.setState({
      showAddRemove: !this.state.showAddRemove,
      index: i,
    });
  };

  openAddRemove2 = async (event, i) => {
    event.preventDefault();
    this.setState({
      showAddRemove2: !this.state.showAddRemove2,
      index: i,
    });
  };

  openAddRemove3 = async (event, i) => {
    event.preventDefault();
    this.setState({
      showAddRemove3: !this.state.showAddRemove3,
      index: i,
    });
  };

  amountChange = async (event, index) => {
    event.preventDefault();
    this.setState({
      amount: event.target.value,
    });

    await this.checkAllowance(index);

    if (
      this.state.allowanceAmount < this.state.amount ||
      this.state.amount == 0
    ) {
      this.setState({
        showAllowance: true,
      });
    } else {
      this.setState({
        showAllowance: false,
      });
    }

    await this.checkAllowance(index);
  };

  checkAllowance = async (index) => {
    var contractInit = new this.state.web3.eth.Contract(
      ERC20Token,
      Tokens[index]
    );

    var decimal = await contractInit.methods.decimals().call();
    var amount = await contractInit.methods
      .allowance(
        this.state.account,
        this.state.oppurtunitiesTokens[index].singleOppurtunityAddress[0]
      )
      .call();

    this.setState({
      allowanceAmount: amount / 10 ** decimal,
    });
  };

  toFixed = (x) => {
    if (Math.abs(x) < 1.0) {
      var e = parseInt(x.toString().split("e-")[1]);
      if (e) {
        x *= Math.pow(10, e - 1);
        x = "0." + new Array(e).join("0") + x.toString().substring(2);
      }
    } else {
      var e = parseInt(x.toString().split("+")[1]);
      if (e > 20) {
        e -= 20;
        x /= Math.pow(10, e);
        x += new Array(e + 1).join("0");
      }
    }
    return x;
  };

  allowance = async (event, index) => {
    event.preventDefault();

    if (this.state.amount != 0) {
      try {
        var prov = new ethers.providers.Web3Provider(this.state.provider);

        var signer = await prov.getSigner(0);

        var contractInit = new ethers.Contract(
          Tokens[index],
          ERC20Token,
          signer
        );

        var amo = await this.toFixed(10 ** 71);

        this.setState({
          modalShown: false,
          showAllowance: false,
        });

        var con = contractInit.approve(
          this.state.oppurtunitiesTokens[this.state.popupIndex]
            .singleOppurtunityAddress[0],
          amo.toString()
        );

        con
          .then(async (data) => {
            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: data.hash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                // gasUsed: data.gasUsed,
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops4: true,
            });

            var set = await prov.waitForTransaction(data.hash);

            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: set.transactionHash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                gasUsed: set.gasUsed.toString(),
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops1: true,
              pops4: false,
            });
          })
          .catch((err) => {
            console.log({ err });
            this.setState({
              pops2: true,
            });
          });
      } catch (error) {
        this.setState((prevState) => ({
          transactionErrors: {
            ...prevState.transactionErrors,
            messageError: "Something went wrong!",
          },
        }));
        this.setState({
          pops2: true,
        });
      }
    } else {
      this.setState((prevState) => ({
        transactionErrors: {
          ...prevState.transactionErrors,
          messageError: "Something went wrong!",
        },
      }));
      this.setState({
        pops2: true,
      });
    }
  };

  earnAmount = async (event) => {
    event.preventDefault();

    if (this.state.amount != 0) {
      try {
        var prov = new ethers.providers.Web3Provider(this.state.provider);

        var signer = await prov.getSigner(0);

        var contractInit = new ethers.Contract(
          this.state.oppurtunitiesTokens[
            this.state.popupIndex
          ].singleOppurtunityAddress[0],
          singleOppurtunity,
          signer
        );

        var amo = await this.toFixed(this.state.amount * 10 ** 18);

        this.setState({
          modalShown: false,
          showAllowance: false,
        });

        var con = contractInit.joinOpportunity(amo.toString())

        con
          .then(async (data) => {
            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: data.hash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                // gasUsed: data.gasUsed,
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));

            this.setState({
              pops4: true,
            });

            var set = await prov.waitForTransaction(data.hash);

            this.startingFunctions();

            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: set.transactionHash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                gasUsed: set.gasUsed.toString(),
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops1: true,
              pops4: false,
            });
          })
          .catch((err) => {
            console.log({ err });
            this.setState({
              pops2: true,
            });
          });
      } catch (error) {
        this.setState((prevState) => ({
          transactionErrors: {
            ...prevState.transactionErrors,
            messageError: "Something went wrong!",
          },
        }));
        this.setState({
          pops2: true,
        });
      }
    } else {
      this.setState((prevState) => ({
        transactionErrors: {
          ...prevState.transactionErrors,
          messageError: "Something went wrong!",
        },
      }));
      this.setState({
        pops2: true,
      });
    }
  };

  tokenIdData = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();

      var tokenIdArray = [];

      for (let i = 0; i < balance; i++) {
        var tokenId = await contractInit.methods
          .tokenOfOwnerByIndex(this.state.account, i)
          .call();

        var singleContractAddress = await contractInit.methods
          .getNftParent(tokenId)
          .call();

        tokenIdArray.push({
          tokenId,
          singleContractAddress,
        });
      }

      this.setState({
        tokenIdDetails: tokenIdArray,
      }, () => {
        this.tokenHistory()
      });
    }
  };

  convertDate = async (data) => {
    var da = new Date(data * 1000);
    return da.getDate() + "-" + da.getMonth() + 1 + "-" + da.getFullYear();
  };

  tokenHistory = async () => {
    var history = [];

    if (this.state.tokenIdDetails.length > 0) {
      for (let i = 0; i < this.state.tokenIdDetails.length; i++) {
        const element = this.state.tokenIdDetails[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element.singleContractAddress
        );

        var singleAssetName = await contractInit.methods.getName().call();

        var status = await singleAssetName
          .toString()
          .toUpperCase()
          .includes(Data.AssetData[0].toString().toUpperCase());

        if (status) {
          var tokenDetails = await contractInit.methods
            .gettokenDetails(element.tokenId)
            .call();

          var contractInit1 = new this.state.web3.eth.Contract(
            ERC20Token,
            tokenDetails.baseToken
          );

          var symbol = await contractInit1.methods.symbol().call();

          var dat = await this.convertDate(tokenDetails.startTimestamp);

          history.push({
            tokenAddress: tokenDetails.baseToken,
            symbol,
            amount: tokenDetails.tokenAmount,
            startTime: dat,
            endTime: tokenDetails.endTimestamp,
            tokenId: tokenDetails.nfttokenId,
            apy: tokenDetails.apy,
          });
        }
      }

      this.setState({
        tokenHistory: history,
      });
    }
  };

  onchangeTokenId = (e) => {
    e.preventDefault();

    this.setState({
      formTokenId: e.target.value,
    });
  };

  withdrawAmount = async (e, id, name) => {
    e.preventDefault();

    this.setState({
      popupToken: name,
    });

    var obj = await this.state.tokenIdDetails.find(
      ({ tokenId }) => tokenId === id
    );

    var prov = new ethers.providers.Web3Provider(this.state.provider);

        var signer = await prov.getSigner(0);

    var contractInit = new ethers.Contract(
      obj.singleContractAddress,
      singleOppurtunity,
      signer
    );

    var con = contractInit.exitOpportunity(id)

    con
          .then(async (data) => {
            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: data.hash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                // gasUsed: data.gasUsed,
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops4: true,
            });

            var set = await prov.waitForTransaction(data.hash);

            this.startingFunctions();

            this.setState((prevState) => ({
              transactionDetails: {
                ...prevState.transactionDetails,
                hash: set.transactionHash,
                // ETA: 1,
                gasLimit: data.gasLimit.toString(),
                gasUsed: set.gasUsed.toString(),
                gasPrice: parseInt(data.gasPrice.toString()) / 10 ** 9,
              },
            }));
            this.setState({
              pops1: true,
              pops4: false,
            });

            // setTimeout(() => {
            //   console.log("inside timeout:::::::::");
            //   this.setState({
            //     pops1: false,
            //     modalShown: true,
            //   });
            // }, 5000);


          })
          .catch((err) => {
            console.log({ err });
            this.setState({
              pops2: true,
            });
          });
  };

  closePopup1 = () => {
    this.setState({ pops1: false });
  };

  closePopup2 = () => {
    this.setState({ pops2: false });
  };

  closePopup4 = () => {
    this.setState({ pops4: false });
  };

  render() {
    const {
      tokenDetails,
      Images,
      tokenHistory,
      oppurtunitiesTokens,
      showAllowance,
      modalShown,
      pops,
      message,
      popupIndex,
      popupToken,
      web3,
      isLoading,
      pops4,
      pops1,
      pops2,
      transactionErrors,
      trDataArray,
      transactionDetails,
      addButton
    } = this.state;
    return (
      // <>
      //   {isLoading ? (
      //     <Loader />
      //   ) : (
      <>
        <>
          {<Header
            propData={null}
            onChangeWeb3={(web3, provider) =>
              this.onChangeWeb3(web3, provider)
            }
          />}
          {pops ? <Popup message={message} /> : <></>}
          {pops1 ? (
            <Completed popupToken={popupToken} popup={pops1} transactionDetails={transactionDetails} closePopup={() => this.closePopup1()} />
          ) : (
            ""
          )}

          {pops2 ? (
            <Failed
              popup={pops2}
              popupToken={popupToken}
              transactionErrors={transactionErrors}
              closePopup={() => this.closePopup2()}
            />
          ) : (
            ""
          )}
          {pops4 ? (
            <Loading popup={pops4} popupToken={popupToken} transactionDetails={transactionDetails} closePopup={() => this.closePopup4()} />
          ) : (
            ""
          )}
          <div className="container-fluid">
            <div className="row ecofriend-div" style={{ marginTop: "2rem" }}>
              {/* ------------------------- */}
              <this.Modal
                shown={modalShown}
                close={() => {
                  this.setState({
                    modalShown: false,
                  });
                }}
              >
                <div className="coins-duoss">
                  <div className="img-coin">
                    <img
                      src={Images[popupIndex]}
                      alt=""
                      className="image-small-coin"
                    />
                  </div>
                  <div className="coin-details">
                    <p> {popupToken} </p>
                  </div>
                </div>
                <div style={{ textAlign: "center" }}>
                  <input
                    style={{ boxShadow: "lightgrey 0px 5px 6px" }}
                    type="text"
                    name=""
                    id=""
                    placeholder="Enter Amount"
                    className="input-popup"
                    disabled={addButton ? true : false}
                    onChange={(e) => this.amountChange(e, popupIndex)}
                  />
                </div>
                <div
                  style={{
                    textAlign: "center",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <button
                    className="approves"
                    style={{
                      display: showAllowance ? "block" : "none",
                      backgroundImage:
                        "linear-gradient(to bottom, #46cbb8, #2fa898)",
                    }}
                    onClick={(e) => this.allowance(e, popupIndex)}
                  >
                    APPROVE
                  </button>
                </div>
                <div
                  style={{
                    textAlign: "center",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <button
                    className="approves"
                    style={{
                      display: !showAllowance ? "block" : "none",
                      backgroundImage:
                        "linear-gradient(to bottom, #46cbb8, #2fa898)",
                    }}
                    onClick={(e) => this.earnAmount(e)}
                  >
                    ADD
                  </button>
                </div>
              </this.Modal>

              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div
                  className="table-responsive"
                  style={{ overflowY: "hidden" }}
                >
                  <table className="table">
                    <tr className="tr-data">
                      <td
                        className="tr-data-head1 tr-data-head-after1"
                        style={{
                          width: "8rem",
                          borderRadius: "10px 0px 0px 10px",
                        }}
                      >
                        BEP20 tokens
                      </td>
                      <td
                        className="tr-data-head tr-data-head-after1"
                        style={{ width: "13rem" }}
                      >
                        Balances
                      </td>
                      {/* <td
                      className="tr-data-head tr-data-head-after1"
                      style={{ width: "10rem" }}
                    >
                      Token Id
                    </td> */}
                      {/* <td
                            className="tr-data-head tr-data-head-after1"
                            style={{ width: "9rem" }}
                          >
                            Locked Vault
                          </td> */}
                      <td
                        className="tr-data-head tr-data-head-after1"
                        style={{ width: "9rem" }}
                      >
                        USD Asset Value{" "}
                      </td>
                      <td
                        className="tr-data-head2 tr-data-head-after1"
                        style={{ width: "6rem" }}
                      >
                        APY
                      </td>
                      <td
                        className="tr-data-head2"
                        style={{
                          width: "9rem",
                          borderRadius: "0px 10px 10px 0px",
                        }}
                      >
                        Action
                      </td>
                    </tr>
                    {trDataArray}
                  </table>
                </div>
              </div>

              {/* <div className="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                <div className="reward-detail">
                  <p className="reward-info">Your Reward Details</p>
                  <div className="eco-number">
                    <div>
                      <img src="/eco.png" alt="" className="eco-number-img" />
                    </div>

                    <div className="num-eco">
                      <p>Number of ECO</p>
                    </div>
                  </div>

                  <div className="eco-number">
                    <div>
                      <img
                        src="/dollar.svg"
                        alt=""
                        className="eco-number-img"
                      />
                    </div>

                    <div className="num-eco">
                      <p>USD Value of Reward</p>
                    </div>
                  </div>
                </div>
              </div> */}

              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div className="my-orders-table">
                  <p className="my-orders-head">My Orders</p>
                </div>
              </div>

              <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div className="table-responsive">
                  <table class="table orders-table-data">
                    <thead>
                      <tr>
                        <th scope="col" className="orders-th-data">
                          Token Id
                        </th>
                        <th scope="col" className="orders-th-datas">
                          Token Symbol
                        </th>
                        <th scope="col" className="orders-th-datas">
                          Amount
                        </th>
                        <th scope="col" className="orders-th-datas">
                          Apy
                        </th>
                        <th scope="col" className="orders-th-data2">
                          Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {tokenHistory.length > 0 ? (
                        <>
                        {console.log('tokenHistory ------------------>', tokenHistory)}
                          {tokenHistory.map((data, i) => {
                            return (
                              <tr key={i} style={{ textAlign: "center" }}>
                                <th scope="row"> {data.tokenId} </th>
                                <td> {data.symbol} </td>
                                <td> {parseInt(data.amount) / 10 ** 18} </td>
                                <td> {parseInt(data.apy) / 100}% </td>
                                <td>
                                  <button
                                    className="remove-btn-history"
                                    onClick={(e) =>
                                      this.withdrawAmount(e, data.tokenId, data.symbol)
                                    }
                                  >
                                    Remove
                                  </button>
                                </td>
                              </tr>
                            );
                          })}
                        </>
                      ) : (
                        <></>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <Footer />
        </>
      </>
      //   )}
      // </>
    );
  }
}

export default withRouter(Treasury);
