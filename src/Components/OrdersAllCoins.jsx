import React, { Component } from "react";
import tokenData from "../Constants/token";
import WBTC from "../images/B.png";
import ECO from "../images/eco.png";
import ETH from "../images/ether.png";
import OMC from "../images/omc.png";
import ORME from "../images/ormeus.png";
import USDT from "../images/busd.svg";
import WBNB from "../images/binance.png";

class OrdersAllCoins extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ImagesPrice: [ECO, OMC, ORME, USDT, WBTC, ETH, WBNB],
      usdValueTokenList: [
        "ormeus-ecosystem",
        "ormeus-cash",
        "ormeuscoin",
        "binance-usd",
        "wrapped-bitcoin",
        "ethereum",
        "binancecoin",
      ],
    };
  }

  render() {
    const { ImagesPrice } = this.state;
    const { data, i } = this.props;
    return (
      <div className="col" key={i}>
        <div className="coins-duo">
          <div className="img-coin">
            <img src={ImagesPrice[i]} alt="" className="image-small-coin" />
          </div>
          <div className="coin-details">
            <p>{tokenData.tokenNames[i].toString().toUpperCase()}</p>
            <p>${data} </p>
          </div>
        </div>
      </div>
    );
  }
}

export default OrdersAllCoins;
