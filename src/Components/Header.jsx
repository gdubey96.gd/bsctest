import React, { Component } from "react";
import "../Styles/header.css";
import Web3 from "web3";
import { NavLink } from "react-router-dom";
import WalletConnectProvider from "@walletconnect/web3-provider";
import { CopyToClipboard } from "react-copy-to-clipboard";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      web3: null,
      account: null,
      modalShown: false,
      accountString: null,
      count: 0,
    };
  }

  Modal({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
      >
        <div
          className="modal-content"
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
        >
          <p
            style={{ textAlign: "right", fontWeight: "500", cursor: "pointer" }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  async componentDidMount() {
    var wallet = await localStorage.getItem("wallet");
    if (wallet == "metamask" && !this.props.propData) {
      var web3 = new Web3(window.ethereum);
      this.props.onChangeWeb3(web3, window.ethereum);
      this.setState({ web3: web3 }, () => {
        this.walletConnect();
      });
    } else {
      if (wallet == "trust" && !this.props.propData) {
        const web3Provider = new WalletConnectProvider({
          rpc: {
            56: "https://bsc-dataseed1.binance.org/",
          },
          chainId: 56,
          bridge: "https://bridge.walletconnect.org",
        });

        var web3 = new Web3(web3Provider);

        web3Provider
          .enable()
          .then((data) => {
            this.props.onChangeWeb3(web3, web3Provider);
            this.setState({ web3: web3 }, () => {
              this.walletConnect();
            });
          })
          .catch((err) => {
            console.log({ err });
          });
      }
      // else {
      //   this.props.onChangeWeb3(this.props.propData);
      //   this.setState({ web3: this.props.propData }, () => {
      //     this.walletConnect();
      //   });
      // }
    }
  }

  walletConnect = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      try {
        var network = await this.state.web3.eth.net.getNetworkType();
        var accounts = await this.state.web3.eth.getAccounts();
        console.log({ accounts });
        // this.setState({ account: "0x21a29a50F66d5E8DB6E1F700d3987e81C21f259c" });
        this.setState({ account: accounts[0] });
        var acc = await accounts[0].substring(0, 4);
        var acc1 = await accounts[0].substring(accounts[0].length - 4);

        this.setState({
          accountString: acc + "......" + acc1,
        });
      } catch (error) {
        console.log({ error });
      }
    }
  };

  render() {
    const { account, accountString, modalShown } = this.state;
    return (
      <>
        <React.StrictMode>
          <div className="container-fluid">
            <div className="row header-data">
              <this.Modal
                shown={modalShown}
                close={() => {
                  this.setState({
                    modalShown: false,
                  });
                }}
              >
                <div
                  className="coins-duoss"
                  style={{ display: "block", maxWidth: "fit-content" }}
                >
                  <p>Add Token</p>
                  <div className="add-main-div">
                    <button
                      className="add-main-div-btn"
                      onClick={() =>
                        window.open("https://bit.ly/3sY9YDD", "_blank")
                      }
                    >
                      ORMEUS COIN (ORME)
                    </button>
                    <button
                      className="add-main-div-btn"
                      onClick={() =>
                        window.open("https://bit.ly/3t81ytz", "_blank")
                      }
                    >
                      ORMEUS CASH (OMC)
                    </button>
                  </div>
                  <div className="add-main-div">
                    <button
                      className="add-main-div-btn"
                      onClick={() =>
                        window.open("https://bit.ly/3tZqvbJ", "_blank")
                      }
                    >
                      ORMEUS ECOSYSTEM (ECO)
                    </button>
                    <button
                      className="add-main-div-btn"
                      onClick={() =>
                        window.open("https://bit.ly/3nrrxLf", "_blank")
                      }
                    >
                      BINANCE-USDT (BUSD)
                    </button>
                  </div>
                  <div className="add-main-div">
                    <button
                      className="add-main-div-btn"
                      onClick={() =>
                        window.open("https://bit.ly/2SU82Qa", "_blank")
                      }
                    >
                      BINANCE-ETH (BETH)
                    </button>
                    <button
                      className="add-main-div-btn"
                      onClick={() =>
                        window.open("https://bit.ly/3dZGjWw", "_blank")
                      }
                    >
                      BINANCE-BTC (BTCB)
                    </button>
                  </div>
                  <div className="add-main-div">
                    <button
                      className="add-main-div-btn"
                      onClick={() =>
                        window.open("https://bit.ly/3sY9YDD", "_blank")
                      }
                    >
                      WRAP-BINANCE (WBNB)
                    </button>
                    {/* <button
                    className="add-main-div-btn"
                    onClick={() =>
                      window.open("https://bit.ly/3t81ytz", "_blank")
                    }
                  >
                    WRAPOMC (eOMC)
                  </button> */}
                  </div>
                  {/* <div className="add-main-div">
                  <button
                    className="add-main-div-btn"
                    onClick={() =>
                      window.open("https://bit.ly/3tZqvbJ", "_blank")
                    }
                  >
                    WRAPECO (eECO)
                  </button>
                  <button
                    className="add-main-div-btn"
                    onClick={() =>
                      window.open("https://bit.ly/3sY9YDD", "_blank")
                    }
                  >
                    WRAPWBTC (eWBTC)
                  </button>
                </div> */}
                </div>
              </this.Modal>
              <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-xs-12">
                <button
                  className="ecorewards"
                  onClick={() =>
                    window.location.replace("https://claim.ecocelium.io/")
                  }
                >
                  {" "}
                  <span>
                    <img
                      src="/star.png"
                      alt=""
                      style={{ height: "1rem", marginBottom: "0.3rem" }}
                    />
                  </span>{" "}
                  My EcoRewards
                </button>
              </div>
              <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                <div className="eco-image" style={{ textAlign: "center" }}>
                  <img
                    className="img-fluid"
                    src="/logo-small.png"
                    alt=""
                    style={{ height: "auto", width: "7rem" }}
                  />
                </div>
              </div>
              <div
                className="col-xxl-4 col-xl-4 col-lg-4 col-md-8 col-sm-8 col-8"
                style={{ paddingRight: "0px" }}
              >
                <div className="address-switch" style={{ fontWeight: "600" }}>
                  <div className="address-bar">
                    <div className="address-bar-below">
                      <div className="text-div">
                        <span>My Address</span>
                      </div>
                      <div className="text-div-right">
                        <span style={{ whiteSpace: "nowrap" }}>
                          {accountString}
                          <CopyToClipboard
                            text={this.state.account}
                          >
                            <i
                              class="fa fa-copy"
                              style={{ marginLeft: "1rem", cursor: "pointer" }}
                              onClick={() => {
                                alert('Copied')
                              }}
                            ></i>
                          </CopyToClipboard>
                        </span>
                      </div>
                    </div>
                    {this.props.referralId ? (
                      <div className="address-bar-below">
                        <div className="text-div">
                          <span>EcoFriend</span>
                        </div>
                        <div className="text-div">
                          <span style={{ whiteSpace: "nowrap" }}>
                            {this.props.referralId.substring(0, 4) +
                              "..." +
                              this.props.referralId.substring(
                                this.props.referralId.length - 4
                              )}
                          </span>
                        </div>
                      </div>
                    ) : (
                      <></>
                    )}
                  </div>
                  <div className="switch">
                    <p className="address-p">
                      <i
                        class="fas fa-plus-circle"
                        style={{ cursor: "pointer" }}
                        onClick={(e) => {
                          e.preventDefault();
                          this.setState({
                            modalShown: !this.state.modalShown,
                          });
                        }}
                      ></i>
                    </p>
                    <NavLink to="/">
                      <p
                        className="power address-p"
                        style={{ borderRadius: "10px" }}
                      >
                        <i
                          class="fas fa-power-off"
                          style={{ color: "white" }}
                        ></i>
                      </p>
                    </NavLink>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.StrictMode>
      </>
    );
  }
}

// export const web3 = this.state.web3;

export default Header;
