import React, { Suspense, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
// const Treasury = React.lazy(() => import("./Treasury"));
// const LandingPage = React.lazy(() => import("./LandingPage"));
// const StartPopup = React.lazy(() => import("./Popup2"));
// const Swap = React.lazy(() => import("./Swap"));
// const Orders = React.lazy(() => import("./Orders"));
// const EcoCerator = React.lazy(() => import("./EcoCerator"));
import LandingPage from "./LandingPage";
// import "../Styles/style.css";
import StartPopup from "./Popup2";
import Treasury from "./Treasury";
import Swap from "./Swap";
import Orders from "./Orders";
import EcoCerator from "./EcoCerator";
import EcoFriend from "./EcoFriend";
import Ecolizer from "./Ecolizer";
// import Loader from "./Loader";

function Routers() {
  const [count, setCount] = useState({});

  return (
    <Router>
      <Switch>
        <Route
          exact
          path="/"
          render={(props) => <LandingPage state={count} setState={setCount} />}
        />
        <Route path="/popup" render={() => <StartPopup />} />
        <Route
          path="/treasury"
          render={(props) => <Treasury state={count} />}
        />
        <Route path="/earn" render={() => <Orders state={count} />} />
        <Route path="/swap" render={() => <Swap state={count} />} />
        <Route path="/ecolizer" render={() => <Ecolizer state={count} />} />
        <Route
          path="/ecocelerator"
          render={() => <EcoCerator state={count} />}
        />
        <Route path="/referral" render={() => <EcoFriend />} />
        {/* <Route path="/loader" component={Loader}></Route> */}
      </Switch>
    </Router>
  );
}

export default Routers;
