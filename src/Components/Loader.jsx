import React, { Component } from "react";
import "../Styles/loader.css";

class Loader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <>
        <div className="loader-div">
          <img id="breathing-button" src="/logo-small.png" alt=""/>
        </div>
      </>
    );
  }
}

export default Loader;
