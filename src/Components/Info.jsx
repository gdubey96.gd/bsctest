import React, { Component } from "react";
import LoudSpeaker from "../images/Path 187@1X.png";

class Info extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pops: false,
    };
  }

  Modal({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
      >
        {/* box-shadow: 6px 8px 9px #e6e6e6;
                f2f2f2 */}
        <div
          className="modal-content"
          style={{
            background: "#f2f2f2",
            borderRadius: "15px",
            width: "30rem",
            margin: "1rem",
          }}
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
        >
          <p
            style={{ textAlign: "right", fontWeight: "500", cursor: "pointer" }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  render() {
    const { pops2 } = this.state;
    const { popup, closePopup } = this.props;
    return (
      <this.Modal
        shown={popup}
        close={() => {
          closePopup();
        }}
      >
        <div className="info-data-home">
          <p>
            For all users that have not added the Binance Smart Chain Network to
            their wallet, please use the <span style={{fontWeight:'700'}}>  orange (+) </span> button to do so.
          </p>
          <p>
            <span style={{fontWeight:'700'}}>NOTE : </span>
            This button will only help you add the Network to your MetaMask
            browser wallet
          </p>
        </div>
      </this.Modal>
    );
  }
}

export default Info;
