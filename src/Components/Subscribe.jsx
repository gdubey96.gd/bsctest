import React, { Component } from "react";

class Subscribe extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  popup_energy = (event, data) => {
    event.preventDefault();
    if (data == "add")
      document
        .getElementById("show_div")
        .classList.add("overlay_flight_traveldil_show");
    else
      document
        .getElementById("show_div")
        .classList.remove("overlay_flight_traveldil_show");
  };
  render() {
    return (
      <>
        <div id="show_div" className="overlay_flight_traveldil overlay_flight_traveldil_show">
          <div className="popup_flight_travlDil">
            <div className="popup">
              <div className="search-box2">
                <p
                  className="popup-close"
                  onClick={(e) => this.popup_energy(e, "cross")}
                >
                  X
                </p>
                <div className="coins-duoss">
                  <div className="coin-details" style={{lineHeight: '1rem'}}>
                    <p className="amount" style={{wordBreak: 'break-word', cursor: 'pointer'}}> Hello <span>
                    <i
                        class="fa fa-copy"
                        style={{ marginLeft: "1rem", cursor: "pointer" }}
                      ></i></span> </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Subscribe;
