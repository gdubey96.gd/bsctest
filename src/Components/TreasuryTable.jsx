import React, { Component } from "react";
import WBTC from "../images/B.png";
import ECO from "../images/eco.png";
import ETH from "../images/ether.png";
import OMC from "../images/omc.png";
import ORME from "../images/ormeus.png";
import USDT from "../images/busd.svg";
import WBNB from "../images/binance.png";

class TreasuryTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Images: [ECO, OMC, ORME, USDT, WBTC, ETH, WBNB],
    };
  }
  render() {
    const { tokenDetails, oppurtunitiesTokens } = this.props;
    const { Images } = this.state;
    return (
      <div className="table-responsive" style={{ overflowY: "hidden" }}>
        <table className="table">
          <tr className="tr-data">
            <td
              className="tr-data-head1 tr-data-head-after1"
              style={{
                width: "8rem",
                borderRadius: "10px 0px 0px 10px",
              }}
            >
              BEP20 tokens
            </td>
            <td
              className="tr-data-head tr-data-head-after1"
              style={{ width: "13rem" }}
            >
              Balances
            </td>
            {/* <td
                      className="tr-data-head tr-data-head-after1"
                      style={{ width: "10rem" }}
                    >
                      Token Id
                    </td> */}
            {/* <td
                            className="tr-data-head tr-data-head-after1"
                            style={{ width: "9rem" }}
                          >
                            Locked Vault
                          </td> */}
            <td
              className="tr-data-head tr-data-head-after1"
              style={{ width: "9rem" }}
            >
              USD Asset Value{" "}
            </td>
            <td
              className="tr-data-head2 tr-data-head-after1"
              style={{ width: "6rem" }}
            >
              APY
            </td>
            <td
              className="tr-data-head2"
              style={{
                width: "9rem",
                borderRadius: "0px 10px 10px 0px",
              }}
            >
              Action
            </td>
          </tr>
          {tokenDetails.length > 0 ? (
            <>
              {tokenDetails.map((data, i) => {
                return (
                  <tr className="tr-data tr-content" key={i}>
                    <td className="tr-data-head-after">
                      <div
                        className="tdha"
                        //   onClick={(e) => this.openAddRemove(e, i)}
                      >
                        <img src={Images[i]} alt="" className="table-img" />
                        <span style={{ cursor: "pointer" }}>{data.symbol}</span>
                      </div>
                    </td>
                    <td
                      className="tr-data-head-after"
                      // onClick={(e) => this.openAddRemove2(e, i)}
                    >
                      <span>{parseFloat(data.balance).toFixed(3)}</span>
                    </td>
                    {/* <td className="tr-data-head-after">
                              <select name="id" id="1">
                              <option value=""></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                              </select>
                            </td> */}
                    {/* <td className="tr-data-head-after">
                                    <span>0</span>
                                  </td> */}
                    <td className="tr-data-head-after">
                      <span> {data.USDAssetValue} </span>
                    </td>
                    <td className="tr-data-head-after">
                      <span>{parseInt(oppurtunitiesTokens[i].apy) / 100}%</span>
                    </td>
                    <td className="tr-data-head-after-button">
                      <button
                        className="remove-btn-history2 treasury-add-button"
                        style={{ padding: "0.5rem" }}
                        //   onClick={(e) => {
                        //     e.preventDefault();
                        //     this.setState({
                        //       modalShown: !this.state.modalShown,
                        //       popupIndex: i,
                        //       popupToken: data.symbol,
                        //     });
                        //   }}
                      >
                        ADD
                      </button>
                    </td>
                  </tr>
                );
              })}
            </>
          ) : (
            <></>
          )}
        </table>
      </div>
    );
  }
}

export default TreasuryTable;
