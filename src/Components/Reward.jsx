import React, { Component } from "react";
import LoudSpeaker from "../images/Path 187@1X.png";
import "../Styles/reward.css";

class Reward extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pops: false,
    };
  }

  Modal({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
      >
        {/* box-shadow: 6px 8px 9px #e6e6e6;
                f2f2f2 */}
        <div
          className="modal-content"
          style={{ borderRadius: "15px", width: "50%" }}
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
        >
          <p
            style={{ textAlign: "right", fontWeight: "500", cursor: "pointer" }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  render() {
    const { pops2 } = this.state;
    const { popup, closePopup } = this.props;
    return (
      <this.Modal
        shown={popup}
        close={() => {
          closePopup();
        }}
      >
        <div className="">
          <div className="reward-box-1">
            <div className="total-reward-p">
              <p>Total Reward</p>
            </div>
            <div className="total-reward-usd">
              <p>0 USD</p>
            </div>
          </div>
          <div
            className="reward-box-2"
            style={{ marginTop: "1rem", color: "#47ccb8" }}
          >
            <div className="total-reward-p">
              <p>Total Reward</p>
            </div>
            <div className="total-reward-usd">
              <p>{this.props.omcQuat} OMC (Approx)</p>
            </div>
          </div>
          <div className="subscribe-button-div">
            <button className="subscribe-button" onClick={() => this.props.withdraw()} >Withdraw Reward</button>
          </div>
        </div>
      </this.Modal>
    );
  }
}

export default Reward;
