import React, { Component } from "react";
import "../Styles/ecocerator.css";
import Header from "./Header";
import Footer from "./Footer";
import oppurtunityNFTContract from "../ABI/oppurtunityNFTContract.json";
import MasterOppurtunity from "../ABI/masterOppurtunity.json";
import singleOppurtunity from "../ABI/singleOppurtunity.json";
import Data from "../Constants/contract";
import ERC20Token from "../ABI/ERC20.json";
import { EcoceleratorTokens } from "../Constants/contract";
import { tokenEcoceleratorNames } from "../Constants/token";
import tokenData from "../Constants/token";
import WBTC from "../images/B.png";
import ECO from "../images/eco.png";
import ETH from "../images/ether.png";
import OMC from "../images/omc.png";
import ORME from "../images/ormeus.png";
import USDT from "../images/busd.svg";
import WBNB from "../images/binance.png";
import Popup from "./Popup";

class EcoToken extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Images: [ECO, OMC, ORME, USDT, WBTC, ETH, WBNB],
      LPImages: [USDT, ECO, OMC, ORME],
      web3: null,
      account: null,
      contractAddress: "0x7E1b859F6C792cABBc2d5581703a3ee1d09674Fd",
      contractInit: null,
      isLoading: false,
      oppurtunities: [],
      oppurtunitiesTokens: [],
      tokenDetails: [],
      nftBalance: null,
      index: null,
      showAllowance: false,
      allowanceAmount: 0,
      tokenIdDetails: [],
      tokenHistory: [],
      formTokenId: null,
      showModal: false,
      modalShown: false,
      modalShown2: false,
      pops: false,
      message: null,
      amount: 0,
      popupIndex: null,
      popupToken: null,
    };
  }

  componentDidMount() {
      console.log("props::::::::::",this.props);
    this.setState(
      {
        web3: this.props.state,
      },
      () => {
        this.startingFunctions();
      }
    );
  }

  
  startingFunctions = async () => {
    this.setState({
      isLoading: true,
    });
    await this.getAccounts();
    await this.initializeContract();
    await this.getAllOppurtunities();
    await this.getAllTokenName();
    await this.getTokenBalance();
    await this.getNFTBalance();
    await this.tokenIdData();
    this.setState({
      isLoading: false,
    });
  };

  
  convertDate = async (data) => {
    var da = new Date(data * 1000);
    var month = (await parseInt(da.getMonth())) + 1;
    return da.getDate() + "-" + month + "-" + da.getFullYear();
  };

  getAccounts = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var network = await this.state.web3.eth.net.getNetworkType();
      var accounts = await this.state.web3.eth.getAccounts();
      // this.setState({ account: "0x21a29a50F66d5E8DB6E1F700d3987e81C21f259c" });
      this.setState({ account: accounts[0] });
      // this.setState({
      //   account: "0x0ee5f030090a6388db1d28f812690c6cc0484359",
      // });
    }
  };

  initializeContract = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      await this.getAccounts();
      var contractInit = new this.state.web3.eth.Contract(
        MasterOppurtunity,
        Data.EcoceleratorMasterOpportunityContract
      );
      this.setState({ contractInit });
    }
  };

  getAllOppurtunities = async () => {
    if (this.state.contractInit != null) {
      var router = await this.state.contractInit.methods
        .getActiveOpportunities()
        .call();
      this.setState({ oppurtunities: router }, () => {
      });
    }
  };

  getAllTokenName = async () => {
    if (this.state.oppurtunities.length > 0) {
      var allTokens = [];

      for (let i = 0; i < this.state.oppurtunities.length; i++) {
        const element = this.state.oppurtunities[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element
        );

        var name = await contractInit.methods.getName().call();

        var tokenName = await this.checkTokenName(name);
        var address = await contractInit.methods.getTokenAddress().call();
        var nftAddress = await contractInit.methods.getNFTAddress().call();
        var apy = await contractInit.methods.getApy().call();

        var obj = {
          name,
          originalName: [],
          address: [],
          singleOppurtunityAddress: [],
          nftAddress: [],
          apy: [],
        };

        obj.name = tokenName;
        obj.originalName.push(name);
        obj.address.push(address);
        obj.singleOppurtunityAddress.push(element);
        obj.nftAddress.push(nftAddress);
        obj.apy.push(apy);

        allTokens.push(obj);
      }

      this.setState({ oppurtunitiesTokens: allTokens }, () => {
      });
    }
  };

  tokenExists = (arr, name) => {
    return arr.some(function (el) {
      return el.name === name;
    });
  };

  checkTokenName = async (data) => {
    var tokenName;
    for (let i = 0; i < tokenData.tokenEcoceleratorNames.length; i++) {
      const element = tokenData.tokenEcoceleratorNames[i];
      var value = await data
        .toString()
        .toUpperCase()
        .includes(element.toUpperCase());
      if (value) {
        tokenName = element;
      }
    }
    return tokenName;
  };

  getTokenBalance = async () => {
    var tokenArray = [];

    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      for (let i = 0; i < EcoceleratorTokens.length; i++) {
        var element = EcoceleratorTokens[i];

        var contractInit = new this.state.web3.eth.Contract(
          ERC20Token,
          element
        );

        var decimal = await contractInit.methods.decimals().call();
        var balance = await contractInit.methods
          .balanceOf(this.state.account)
          .call();
        var symbol = await contractInit.methods.symbol().call();

        tokenArray.push({
          decimal,
          balance: balance / 10 ** decimal,
          symbol,
        });
      }
      this.setState({
        tokenDetails: tokenArray,
      });

      await this.tokenIdData();
    }
  };

  getNFTBalance = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();

      this.setState({
        nftBalance: balance,
      });
    }
  };

  amountChange = async (event, index) => {
    event.preventDefault();
    this.setState({
      amount: event.target.value,
    });

    await this.checkAllowance(index);

    if (
      this.state.allowanceAmount < this.state.amount ||
      this.state.amount == 0
    ) {
      this.setState({
        showAllowance: true,
      });
    } else {
      this.setState({
        showAllowance: false,
      });
    }

    await this.checkAllowance(index);
  };

  checkAllowance = async (index) => {
    var contractInit = new this.state.web3.eth.Contract(
      ERC20Token,
      EcoceleratorTokens[index]
    );

    if (index > 1) {
      var decimal = await contractInit.methods.decimals().call();
      var amount = await contractInit.methods
        .allowance(
          this.state.account,
          Data.LPTokens[index == 2 ? 0 : index == 3 ? 1 : index == 4 ? 2 : ""]
        )
        .call();

      this.setState({
        allowanceAmount: amount / 10 ** decimal,
      });
    } else {
      var decimal = await contractInit.methods.decimals().call();
      var amount = await contractInit.methods
        .allowance(
          this.state.account,
          this.state.oppurtunitiesTokens[index].singleOppurtunityAddress[0]
        )
        .call();

      this.setState({
        allowanceAmount: amount / 10 ** decimal,
      });
    }
  };

  toFixed = (x) => {
    if (Math.abs(x) < 1.0) {
      var e = parseInt(x.toString().split("e-")[1]);
      if (e) {
        x *= Math.pow(10, e - 1);
        x = "0." + new Array(e).join("0") + x.toString().substring(2);
      }
    } else {
      var e = parseInt(x.toString().split("+")[1]);
      if (e > 20) {
        e -= 20;
        x /= Math.pow(10, e);
        x += new Array(e + 1).join("0");
      }
    }
    return x;
  };

  allowance = async (event, index) => {
    event.preventDefault();

    if (this.state.amount != 0) {
      try {
        var contractInit = new this.state.web3.eth.Contract(
          ERC20Token,
          EcoceleratorTokens[index]
        );

        var decimal = contractInit.methods.decimals().call();
        var balance = contractInit.methods.balanceOf(this.state.account).call();

        var amo = await this.toFixed(10 ** 71);

        var singleOpportunity =
          index > 1
            ? Data.LPTokens[
                index == 2 ? 0 : index == 3 ? 1 : index == 4 ? 2 : ""
              ]
            : this.state.oppurtunitiesTokens[this.state.popupIndex]
                .singleOppurtunityAddress[0];

        contractInit.methods
          .approve(singleOpportunity, amo.toString())
          .send({ from: this.state.account })
          .on("transactionHash", (hash) => {
            this.setState({
              pops: true,
              message: hash,
            });
            setTimeout(() => {
              this.setState({
                pops: false,
              });
            }, 5000);
          })
          .on("receipt", (receipt) => {
            this.setState({
              pops: true,
              message: receipt.transactionHash,
            });
            setTimeout(() => {
              this.setState({
                pops: false,
              });
            }, 5000);

            this.startingFunctions();
          })
          .on("error", (error) => {
            this.setState({
              pops: true,
              message: error.message
                ? `Error - ${error.message}`
                : `Error - ${error.receipt.transactionHash}`,
            });
            setTimeout(() => {
              this.setState({
                pops: false,
              });
            }, 5000);
          });
      } catch (error) {
        this.setState({
          pops: true,
          message: "Something went wrong",
        });
        setTimeout(() => {
          this.setState({
            pops: false,
          });
        }, 5000);
      }
    } else {
      this.setState({
        pops: true,
        message: "Please Check",
      });
      setTimeout(() => {
        this.setState({
          pops: false,
        });
      }, 5000);
    }
  };

  earnAmount = async (event) => {
    event.preventDefault();

    if (this.state.amount != 0) {
      try {
        if (this.state.popupIndex > 1) {
          var contractInit = new this.state.web3.eth.Contract(
            singleOppurtunity,
            Data.LPTokens[
              this.state.popupIndex == 2
                ? 0
                : this.state.popupIndex == 3
                ? 1
                : this.state.popupIndex == 4
                ? 2
                : ""
            ]
          );
        } else {
          var contractInit = new this.state.web3.eth.Contract(
            singleOppurtunity,
            this.state.oppurtunitiesTokens[
              this.state.popupIndex
            ].singleOppurtunityAddress[0]
          );
        }

        var amo = await this.toFixed(this.state.amount * 10 ** 18);

        contractInit.methods
          .joinOpportunity(amo.toString())
          .send({ from: this.state.account })
          .on("transactionHash", (hash) => {
            this.setState({
              pops: true,
              message: hash,
            });
            setTimeout(() => {
              this.setState({
                pops: false,
              });
            }, 5000);
          })
          .on("receipt", (receipt) => {
            this.setState({
              pops: true,
              message: receipt.transactionHash,
            });
            setTimeout(() => {
              this.setState({
                pops: false,
              });
            }, 5000);

            this.startingFunctions();
          })
          .on("error", (error) => {
            this.setState({
              pops: true,
              message: error.message
                ? `Error - ${error.message}`
                : `Error - ${error.receipt.transactionHash}`,
            });
            setTimeout(() => {
              this.setState({
                pops: false,
              });
            }, 5000);
          });
      } catch (error) {
        this.setState({
          pops: true,
          message: "Something went wrong",
        });
        setTimeout(() => {
          this.setState({
            pops: false,
          });
        }, 5000);
      }
    } else {
      this.setState({
        pops: true,
        message: "Something went wrong",
      });
      setTimeout(() => {
        this.setState({
          pops: false,
        });
      }, 5000);
    }
  };

  tokenIdData = async () => {
    if (this.state.web3 != null && Object.keys(this.state.web3).length != 0) {
      var contractInit = new this.state.web3.eth.Contract(
        oppurtunityNFTContract,
        Data.NFTContract
      );

      var balance = await contractInit.methods
        .balanceOf(this.state.account)
        .call();

      var tokenIdArray = [];

      for (let i = 0; i < balance; i++) {
        var tokenId = await contractInit.methods
          .tokenOfOwnerByIndex(this.state.account, i)
          .call();

        var singleContractAddress = await contractInit.methods
          .getNftParent(tokenId)
          .call();

        tokenIdArray.push({
          tokenId,
          singleContractAddress,
        });
      }

      this.setState({
        tokenIdDetails: tokenIdArray,
      });

      await this.tokenHistory();
    }
  };

  tokenHistory = async () => {
    var history = [];

    if (this.state.tokenIdDetails.length > 0) {
      for (let i = 0; i < this.state.tokenIdDetails.length; i++) {
        const element = this.state.tokenIdDetails[i];

        var contractInit = new this.state.web3.eth.Contract(
          singleOppurtunity,
          element.singleContractAddress
        );

        var singleAssetName = await contractInit.methods.getName().call();

        var status = await singleAssetName
          .toString()
          .toUpperCase()
          .includes(Data.AssetData[2].toString().toUpperCase());

        if (status) {
          var tokenDetails = await contractInit.methods
            .gettokenDetails(element.tokenId)
            .call();

          var contractInit1 = new this.state.web3.eth.Contract(
            ERC20Token,
            tokenDetails.baseToken
          );

          var symbol = await contractInit1.methods.symbol().call();

          var dat = await this.convertDate(tokenDetails.startTimestamp);

          history.push({
            tokenAddress: tokenDetails.baseToken,
            symbol,
            amount: tokenDetails.tokenAmount,
            startTime: dat,
            endTime: tokenDetails.endTimestamp,
            tokenId: tokenDetails.nfttokenId,
            apy: tokenDetails.apy,
          });
        }
      }

      this.setState({
        tokenHistory: history,
      });
    }
  };

  onchangeTokenId = (e) => {
    e.preventDefault();

    this.setState({
      formTokenId: e.target.value,
    });
  };

  withdrawAmount = async (id) => {
    if (this.state.popupIndex > 1) {
      var contractInit = new this.state.web3.eth.Contract(
        singleOppurtunity,
        Data.LPTokens[
          this.state.popupIndex == 2
            ? 0
            : this.state.popupIndex == 3
            ? 1
            : this.state.popupIndex == 4
            ? 2
            : ""
        ]
      );
    } else {
      var contractInit = new this.state.web3.eth.Contract(
        singleOppurtunity,
        this.state.oppurtunitiesTokens[
          this.state.popupIndex
        ].singleOppurtunityAddress[0]
      );
    }

    contractInit.methods
      .exitOpportunity(id)
      .send({ from: this.state.account })
      .on("transactionHash", (hash) => {
        this.setState({
          pops: true,
          message: hash,
        });
        setTimeout(() => {
          this.setState({
            pops: false,
          });
        }, 5000);
      })
      .on("receipt", (receipt) => {
        this.setState({
          pops: true,
          message: receipt.transactionHash,
        });
        setTimeout(() => {
          this.setState({
            pops: false,
          });
        }, 5000);

        this.startingFunctions();
      })
      .on("error", (error) => {
        this.setState({
          pops: true,
          message: error.message
            ? `Error - ${error.message}`
            : `Error - ${error.receipt.transactionHash}`,
        });
        setTimeout(() => {
          this.setState({
            pops: false,
          });
        }, 5000);
      });
  };

  render() {
    const {
      tokenDetails,
      nftBalance,
      index,
      Images,
      tokenHistory,
      oppurtunitiesTokens,
      showAllowance,
      tokenIdDetails,
      formTokenId,
      modalShown,
      modalShown2,
      popupIndex,
      popupToken,
      pops,
      message,
      web3,
      LPImages,
      isLoading,
    } = this.state;
    return (
      <>
        <div className="main-eco-div">
          <div className="row">
            {tokenDetails.length > 0 ? (
              <>
                {tokenDetails.map((data, i) => {
                  return (
                    <>
                      {i < 4 ? (
                        <>
                          <div
                            className="col-xxl-4 col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12"
                            key={i}
                          >
                            <div className="row eco-div">
                              <div className="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-4">
                                <div
                                  className="img-coin"
                                  style={{
                                    width: i < 1 ? "auto" : "6.5rem",
                                  }}
                                >
                                  {i < 1 ? (
                                    <img
                                      src={i == 0 ? USDT : Images[i]}
                                      alt=""
                                      className="image-small-coins"
                                    />
                                  ) : (
                                    <div style={{ display: "flex" }}>
                                      <img
                                        src={LPImages[i]}
                                        alt=""
                                        className="image-small-coins"
                                        style={{
                                          zIndex: "0",
                                          height: "3rem",
                                          width: "3rem",
                                        }}
                                      />
                                      <img
                                        src={USDT}
                                        alt=""
                                        className="image-small-coins"
                                        style={{
                                          zIndex: "1",
                                          marginLeft: "-36%",
                                          marginTop: "1.3rem",
                                          height: "3rem",
                                          width: "3rem",
                                        }}
                                      />
                                    </div>
                                  )}
                                </div>
                              </div>
                              <div
                                className="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8"
                                style={{
                                  lineHeight: "10px",
                                  marginLeft: i < 2 ? "" : "-1rem",
                                }}
                              >
                                <p className="text-head">
                                  {tokenEcoceleratorNames[i]
                                    .toString()
                                    .toUpperCase()}
                                </p>
                                <p className="text-center">
                                  <span className="eco-heading-span">APY:</span>
                                  <span className="eco-data-span">
                                    {parseInt(oppurtunitiesTokens[i].apy) / 100}
                                    %
                                  </span>
                                </p>
                                <p
                                  className="text-center"
                                  style={{ textAlign: "left" }}
                                >
                                  <span className="eco-heading-span">
                                    Minimum Period:{" "}
                                  </span>{" "}
                                  <br />{" "}
                                  <span className="eco-data-span">7 Days</span>
                                </p>
                                <p className="text-center eco-heading-span">
                                  Your Balance:
                                </p>
                                <p>{parseFloat(data.balance).toFixed(3)} </p>
                              </div>
                              <div
                                className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"
                                style={{ textAlign: "center" }}
                              >
                                <button
                                  className="approve"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.setState({
                                      modalShown: !this.state.modalShown,
                                      popupIndex: i,
                                      popupToken: data.symbol,
                                    });
                                  }}
                                >
                                  ADD
                                </button>
                                <button
                                  className="remove"
                                  onClick={(e) => {
                                    e.preventDefault();
                                    this.setState({
                                      modalShown2: !this.state.modalShown2,
                                      popupIndex: i,
                                      popupToken: data.symbol,
                                    });
                                  }}
                                >
                                  REMOVE
                                </button>
                              </div>
                            </div>
                          </div>
                        </>
                      ) : (
                        <></>
                      )}
                    </>
                  );
                })}
              </>
            ) : (
              <></>
            )}
          </div>
        </div>
      </>
    );
  }
}

export default EcoToken;
