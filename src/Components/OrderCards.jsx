import React, { Component } from "react";
import WBTC from "../images/B.png";
import ECO from "../images/eco.png";
import ETH from "../images/ether.png";
import OMC from "../images/omc.png";
import ORME from "../images/ormeus.png";
import USDT from "../images/busd.svg";
import WBNB from "../images/binance.png";

class OrderCards extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Images: [ECO, ORME, USDT, WBTC, ETH, WBNB, OMC],
      tokenRadioTokenIndex: null
    };
  }

  radioOnChange = (i) => {
    this.setState({
      tokenRadioTokenIndex: i
    })
  }

  render() {
    const { data, i, apyData } = this.props;
    const { Images } = this.state;
    return (
      <div
        className="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12"
        key={i}
        onClick={(e) => {
          this.radioOnChange(i)
          this.props.tokenClick(e, data.name, i)
        }}
      >
        {console.log('i ----------------------------------------->', i)}
        <input
          class="checkbox-tools"
          type="radio"
          name="tools"
          id="tool-1"
          checked={this.state.tokenRadioTokenIndex == i}
        ></input>
        <div className="apy-box-main" tabIndex="1">
          <div className="apy-box-main-1">
            <p className="add-btn">Deposit</p>
          </div>
          <div className="coins-duos" style={{ padding: "0.5rem" }}>
            <div className="img-coin">
              <img src={Images[i]} alt="" className="image-small-coin" />
            </div>
            <p className="heading-orders">{data.name.toUpperCase()}</p>
            <p className="earn-eco">Earn ECO </p>
          </div>
          <div className="apy-box">
            <p className="apy">
              <span
                style={{
                  visibility:
                    this.props.apyText && this.props.tokenRadioTokenIndex == i
                      ? "visible"
                      : "hidden",
                }}
              >
                APY
              </span>
              <span>
                {this.props.apyPercentage &&
                this.props.tokenRadioTokenIndex == i
                  ? parseInt(this.props.apyData) / 100
                  : 0}
                %
              </span>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default OrderCards;
