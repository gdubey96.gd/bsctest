import React, { Component } from "react";
import "../Styles/historyPopup.css";

class HistoryPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pops: false,
    };
  }

  Modal({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
      >
        {/* box-shadow: 6px 8px 9px #e6e6e6;
                f2f2f2 */}
        <div
          className="modal-content"
          style={{ borderRadius: "15px", width: "50%" }}
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
        >
          <p
            style={{ textAlign: "right", fontWeight: "500", cursor: "pointer" }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  render() {
    const { pops2 } = this.state;
    const { popup, closePopup } = this.props;
    return (
      <this.Modal
        shown={popup}
        close={() => {
          closePopup();
        }}
      >
        <div className="history-popup">
          <div>
            <p className="reward-history">Reward History</p>
          </div>
          <div className="table-responsive" style={{ overflowY: "hidden" }}>
            <table class="table">
              <thead className="history-head">
                <tr>
                  <th scope="col" className=" history-head date-head">
                    Date
                  </th>
                  <th scope="col" className="history-head">
                    Removed <br /> (USD)
                  </th>
                  <th scope="col" className="history-head">
                    Removed <br /> (OMC)
                  </th>
                  <th scope="col" className="history-head">
                    Status
                  </th>
                  <th scope="col" className=" history-head hashcode-head">
                    Hash Code
                  </th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </this.Modal>
    );
  }
}

export default HistoryPopup;
