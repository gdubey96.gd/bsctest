import React, { Component } from "react";
import LoudSpeaker from "../images/Path 187@1X.png";
import  "../Styles/newsletter.css";

class Newsletter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pops: false,
    };
  }

  Modal({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          close();
        }}
      >
        <div
          className="modal-content"
          style={{ background: "#f2f2f2", borderRadius: "15px" , margin:'1rem' }}
          onClick={(e) => {
            e.stopPropagation();
          }}
        >
          <p
            style={{ textAlign: "right", fontWeight: "500", cursor: "pointer" }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  render() {
    const { pops2 } = this.state;
    const { popup, closePopup } = this.props;
    return (
      <this.Modal
        shown={popup}
        close={() => {
          closePopup();
        }}
      >
        <div className="" style={{ padding: "0.5rem", display: "flex" }}>
          <img src={LoudSpeaker} alt="" srcset="" style={{ margin: "10px" }} />
          <p>
            Subscribe to our newsletter <br />
            <span className="subscribe-div-span">
              Updates, promotions and news. Directly to your inbox.
            </span>
          </p>
        </div>
        <div
          style={{
            textAlign: "center",
            background: "white",
            borderRadius: "10px",
            margin: "0.5rem",
            boxShadow: "6px 8px 9px #e6e6e6",
          }}
        >
          <input
            type="text"
            name=""
            id=""
            placeholder="Your Email"
            className="input-popups"
            onChange={(e) => {
              e.preventDefault();
              this.props.handleChange(e.target.value);
            }}
          />
        </div>
        <div style={{ textAlign: "center", marginTop: "3px" }}>
          <button
            className="subscribe-button"
            onClick={(e) => {
              e.preventDefault();
              this.props.subscribe();
            }}
          >
            SUBSCRIBE
          </button>
        </div>
      </this.Modal>
    );
  }
}

export default Newsletter;
