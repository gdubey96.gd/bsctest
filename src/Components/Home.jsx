import React from 'react'

function Home() {
  return (
    <>
      <div className="responsive">
        <img src="/landing.png" alt="" className="landing-page" />
        <div className="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div className="landpage-data">
            <p className="welcome">Welcome to EcoCelium</p>
            <p className="defi">DeFi made simple</p>
            <div className="logo-image">
            <img src="/logo.jpg" style={{height:'18rem',width:'18rem'}} className="responsive"/>
            </div>
            <div className="btn-btn-call">
              <button className="btn-launch">Launch EcoCelium</button>
              <button className="btn-launch" style={{marginLeft:'4rem'}}>Go To Infopage</button>
            </div>
            <div className="subscribe-btn">
              <button className="btn-launch subscribe">Subscribe to our newsletter</button>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Home
