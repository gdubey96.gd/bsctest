import React, { Component } from "react";
import { ImagesDetails } from "../Constants/token";
import "../Styles/ecocerator.css";
import "../Styles/completed.css";
import "../Styles/failed.css";

class Failed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pops2: false,
    };
  }

  Modal({ children, shown, close }) {
    return shown ? (
      <div
        className="modal-backdrop"
        onClick={() => {
          // close modal when outside of modal is clicked
          close();
        }}
      >
        <div
          className="modal-content"
          style={{
            background: "#f2f2f2",
            borderRadius: "22px",
            height: "15.5rem",
            width: "25rem",
            // padding: "3rem",
            margin:'1rem'
          }}
          onClick={(e) => {
            // do not close modal if anything inside modal content is clicked
            e.stopPropagation();
          }}
        >
          <p
            style={{
              textAlign: "right",
              fontWeight: "500",
              cursor: "pointer",
              marginBottom: "0px",
            }}
            onClick={close}
          >
            X
          </p>
          {children}
        </div>
      </div>
    ) : null;
  }

  render() {
    const { popup, closePopup, transactionErrors, popupToken } = this.props;
    return (
      <this.Modal
        shown={popup}
        close={() => {
          closePopup();
        }}
      >
        <div className="">
          
          <div className="eco-div eco-div-tick">
            <div style={{ display: "flex", padding: "0.2rem", position: 'relative' }}>
                <div className="uchka-hua-popup"></div>
              <div style={{zIndex: '3'}}>
              <img src={ImagesDetails.find(o => o.name.toUpperCase() == popupToken.toUpperCase()).image} alt="" className="action-popup" />
              </div>
              
              <div
                style={{
                  lineHeight: "0.3rem",
                  alignItems: "baseline",
                  marginTop: "1rem",
                  marginLeft:'1rem'
                }}
              >
                <p> {popupToken.toUpperCase()} </p>
                {/* <p>25.2831</p> */}
              </div>
            </div>
            <div>
            <i class="fas fa-exclamation-triangle alert-triangle"></i>
            </div>
          </div>
          <div className="trans-failed-text">
            <p className="trans-complete"> {transactionErrors.message} </p>
            <p> {transactionErrors.messageError} </p>
          </div>
          
        </div>
        
      </this.Modal>
    );
  }
}

export default Failed;
