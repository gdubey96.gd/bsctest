module.exports = {
    tokenNames: ['eco', 'omc', 'orme', 'busd', 'btcb', 'eth', 'wbnb'],
    tokenEcoceleratorNames: ['busd', 'lp-eco-busd', 'lp-omc-busd', 'lp-orme-busd'],
    tokenMonths: [6, 12, 18, 24, 30, 36, 48, 60],
    ImagesDetails: [
        {
            name: 'eco', image: "images/eco.png"
        },
        {
            name: 'omc', image: "images/omc.png"
        },
        {
            name: 'orme', image: "images/ormeus.png"
        },
        {
            name: 'busd', image: "images/busd.svg"
        },
        {
            name: 'btcb', image: "images/B.png"
        },
        {
            name: 'eth', image: "images/ether.png"
        },
        {
            name: 'wbnb', image: "images/binance.png"
        }
    ]
}

// ECO
// ORME
// OMC
// BUSD
// WBNB
// ETH
// BTCB