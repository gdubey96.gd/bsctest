// master - single opportunity - nft contract - token

module.exports = {
    Tokens: [
      "0xeDe2F059545e8Cde832d8Da3985cAacf795B8765", //ECOToken
      "0x5D2F9a9DF1ba3C8C00303D0b4C431897eBc6626A", //OMCToken
      "0x7e2afe446a30fa67600a5174df7f4002b8e15b03", //ORMEToken
      "0xe9e7cea3dedca5984780bafc599bd69add087d56", //USDTToken
      "0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c", //WBTCToken
      "0x2170ed0880ac9a755fd29b2688956bd959f933f8", //ETHToken
      "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c", //WBNBToken
    ],
    TokensEarn: [
      "0xeDe2F059545e8Cde832d8Da3985cAacf795B8765", //ECOToken
      "0x7e2afe446a30fa67600a5174df7f4002b8e15b03", //ORMEToken
      "0xe9e7cea3dedca5984780bafc599bd69add087d56", //USDTToken
      "0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c", //WBTCToken
      "0x2170ed0880ac9a755fd29b2688956bd959f933f8", //ETHToken
      "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c", //WBNBToken
      "0x5D2F9a9DF1ba3C8C00303D0b4C431897eBc6626A", //OMCToken
    ],
  
    NFTContract: "0xEd0407546F28944fB52fE32Ef4FF3391F8917b07",
  
    EarnMasterOpportunity: "0x572C8079778A53Ca748d115953B5c33993f79263",
  
    TreasuryMasterOpportunityContract:
      "0xC0DE0fd325E2eb7E2fa93B18f0900f870078F311",
  
    EcoceleratorMasterOpportunityContract:
      "0x7228ADF748A1DD63A0fC007358a2E00ae5B2306b",
  
    EcoceleratorTokens: [
      // "0xeDe2F059545e8Cde832d8Da3985cAacf795B8765", // token address
      "0xe9e7cea3dedca5984780bafc599bd69add087d56", // busd
      "0x29b1656071e85f7c7b748904810f82ccbf61d710",
      "0xf4f60e3726890f5d213d2b8343112c2f2325e250", // 
      "0xd0dfd6274e48ca344c4c904352818f790d10ecf1",
    ],
    LPTokens: [
      "0xAbf23a5C1018b209dD83F423C76154cfbE9Ef116",
      "0x1Ea2c1c021100eE61a543EEb0cE4847D11500ed9", // single oppurtunity
      "0x243723239917da8B9a525031F15a9fd7F1f6745D",
    ],
    AssetData: ["treasury", "earn", "Ecocelerator"],
  };
  
  // ---------------------- main -----------------------------
  
  // EcoceleratorEco - 0xdA2d02D14365411bb3A10662aD6F117A79E1B125
  // 	EcoceleratorEcoVault - 0x1cd126eF20Eb781A23b1aA0D57444AF88fF54Ece
  
  // EcoceleratorBusd - 0x3B99CDf23B223722399b40F0C6960E9Ce61b7eB2
  // 	EcoceleratorBusdVault - 0x0cBbe82268549011125b39D10aeb51717FA6D59B
  
  // EcoceleratorLpEcoBusd - 0xAbf23a5C1018b209dD83F423C76154cfbE9Ef116
  // 	EcoceleratorLpEcoBusdVault - 0x78726E6EdeF1A37AB9c2008458a9BAeeBf440113
  
  // EcoceleratorLpOmcBusd - 0x1Ea2c1c021100eE61a543EEb0cE4847D11500ed9
  // 	EcoceleratorLpOmcBusdVault - 0x9896B084CE85Ea53566EdCAe41A8a79A7173F3fB
  
  // EcoceleratorLpOrmeBusd -0x243723239917da8B9a525031F15a9fd7F1f6745D
  // 	EcoceleratorLpOrmeBusdVault - 0x741914ca69A03eb317129aC274e5bC1e32b369e4
  
  // TOKEN
  // ECO - 0xeDe2F059545e8Cde832d8Da3985cAacf795B8765 - https://bscscan.com/token/0xeDe2F059545e8Cde832d8Da3985cAacf795B8765
  
  // OMC - 0x5D2F9a9DF1ba3C8C00303D0b4C431897eBc6626A - https://bscscan.com/token/0x5D2F9a9DF1ba3C8C00303D0b4C431897eBc6626A
  
  // OrmeusCoin - 0x7e2afe446a30fa67600a5174df7f4002b8e15b03 - https://bscscan.com/token/0x7e2afe446a30fa67600a5174df7f4002b8e15b03?a=0x8F68D208179eC82aE7c6F6D945262bA478c3d7a7
  
  // BUSD - 0xe9e7cea3dedca5984780bafc599bd69add087d56 - https://bscscan.com/token/0xe9e7cea3dedca5984780bafc599bd69add087d56
  
  // BTCB - 0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c -  https://bscscan.com/token/0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c
  
  // ETH - 0x2170ed0880ac9a755fd29b2688956bd959f933f8 - https://bscscan.com/token/0x2170ed0880ac9a755fd29b2688956bd959f933f8
  
  // WBNB - 0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c - https://bscscan.com/token/0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c
  
  // 0x3b99cdf23b223722399b40f0c6960e9ce61b7eb2, 0xabf23a5c1018b209dd83f423c76154cfbe9ef116, 0x1ea2c1c021100ee61a543eeb0ce4847d11500ed9, 0x243723239917da8b9a525031f15a9fd7f1f6745d
  
  // PancackeLp EcoBusd - 0x29b1656071e85f7c7b748904810f82ccbf61d710 - https://bscscan.com/token/0x29b1656071e85f7c7b748904810f82ccbf61d710#readContract
  // PancackeLp OmcBusd - 0xf4f60e3726890f5d213d2b8343112c2f2325e250 - https://bscscan.com/token/0xf4f60e3726890f5d213d2b8343112c2f2325e250#readContract
  // PancackeLp OrmeBusd - 0xd0dfd6274e48ca344c4c904352818f790d10ecf1 - https://bscscan.com/token/0xd0dfd6274e48ca344c4c904352818f790d10ecf1#readContract
  
  //----------------------- test --------------------------------
  
  // 0xC0846ffB74B636872Df7E7bcF3A098677BF701E7 Treasuryeco
  // 0x5271aAf2051D5e3C4efb6b8E81F2A555D8312bEb nft address
  // 0x5B65909A3813F889C2Fe73057fAF4B6a21394928 master
  // 0x7E1b859F6C792cABBc2d5581703a3ee1d09674Fd token eco
  // 0xE847d6f41aDE1DF2986295b3A2d514F0f9Cbcb8F EcoceleratorEco
  // 0xcB1f1ad971a287db4328cB92b72d4549cD404Abc master
  // 0x5271aAf2051D5e3C4efb6b8E81F2A555D8312bEb nft address
  // 0x7E1b859F6C792cABBc2d5581703a3ee1d09674Fd token eco
  